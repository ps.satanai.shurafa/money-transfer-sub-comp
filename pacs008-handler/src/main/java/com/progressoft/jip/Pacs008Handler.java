package com.progressoft.jip;

import com.progressoft.jip.entities.Messages;
import com.progressoft.jip.entities.MessagesRepository;

import java.io.File;
import java.net.URISyntaxException;

public class Pacs008Handler {
    private final Parser parser;
    private final Serializer serializer;
    private final ValuesValidator validator;
    private final MessagesRepository messagesRepository;

    public Pacs008Handler(Serializer serializer, Parser parser, ValuesValidator validator, MessagesRepository messagesRepository) {
        this.parser = parser;
        this.validator = validator;
        this.serializer = serializer;
        this.messagesRepository = messagesRepository;
    }

    public File handle(File xml) {
        FileValidator(xml);
        String pacs008xml = readFileContent(xml);
        ResponseData response = handle(pacs008xml);
        File res = null;
        if (response.getXml() != null) {
            res = getResponseFile(response);
        }
        return res;
    }

    private String readFileContent(File xml) {
        FileReader reader = new FileReader();
        return reader.read(xml);
    }

    private void FileValidator(File xml) {
        if (isNull(xml))
            throw new NullPointerException("Xml file cannot be null");
        if (xml.isDirectory())
            throw new IllegalArgumentException("File cannot be a directory");
        if (!xml.exists())
            throw new IllegalArgumentException("File does not exist");
    }

    public ResponseData handle(String xml) throws IllegalStateException {
        Pacs008 pacs008;
        RejectionMotives rejectionMotive;
        String reasonDesc;

        checkIfNull(xml);

        try {
            if (isValidStructure(xml)) {
                pacs008 = parse(xml, parser);
                rejectionMotive = validateValues(pacs008, validator);
                reasonDesc = validator.getReasonDesc();

                insertMsgIntoDB(pacs008);

                return getResponseData(pacs008, rejectionMotive, reasonDesc);
            }
        } catch (Exception e) {
            return getFailureResponse();
        }

        return getFailureResponse();
    }

    private void insertMsgIntoDB(Pacs008 pacs008) {
        if (isNotDuplicateMsg()) {
            insertMsgIdIntoDB(pacs008);
        }
    }

    private ResponseData getFailureResponse() {
        ResponseData failureResponse = new ResponseData();
        failureResponse.setXml("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<Error>INVALID PACS MESSAGE!!!</Error>");
        failureResponse.setResponseType("No Response");
        failureResponse.setDesc("Something went wrong .. Check the message structure");

        return failureResponse;
    }

    private void checkIfNull(String xml) {
        if (isNull(xml))
            throw new NullPointerException("Xml cannot be null");
    }

    private ResponseData getResponseData(Pacs008 pacs008, RejectionMotives rejectionMotive, String reasonDesc) {
        ResponseData responseData = new ResponseData();

        responseData.setXml(getResponse(pacs008, serializer, rejectionMotive, reasonDesc));
        responseData.setMsgId(pacs008.getMessageID());

        setResponseType(rejectionMotive, reasonDesc, responseData);

        return responseData;
    }

    private void setResponseType(RejectionMotives rejectionMotive, String reasonDesc, ResponseData responseData) {
        if (isNAUT(rejectionMotive)) {
            responseData.setResponseType("NAUT Pacs002");
            responseData.setDesc(reasonDesc);
        } else {
            responseData.setResponseType("AUTH Pacs002");
        }
    }

    private boolean isNAUT(RejectionMotives rejectionMotive) {
        return rejectionMotive != null;
    }

    private boolean isNotDuplicateMsg() {
        return !validator.isDuplicateMsg();
    }

    private String getResponse(Pacs008 pacs008, Serializer serializer, RejectionMotives rejectionMotive, String reasonDesc) {
        ResponseGenerator generator = new ResponseGenerator();
        Pacs002 pacs002 = generator.generate(pacs008, rejectionMotive, reasonDesc);

        return serializer.serialize(pacs002);
    }

    private void insertMsgIdIntoDB(Pacs008 pacs008) {
        String msgId = pacs008.getMessageID();
        Messages messages = new Messages();
        messages.setID(msgId);
        messagesRepository.save(messages);
    }


    private boolean isNull(Object xml) {
        return xml == null;
    }

    private File getResponseFile(ResponseData response) {
        File responseFile = new File("responses/response-" + response.getMsgId() + ".xml");

        FileWriter writer = new FileWriter();
        writer.write(responseFile, response.getXml());
        return responseFile;
    }

    private boolean isValidStructure(String xml) throws URISyntaxException {
        Pacs008StructureValidator structureValidator = new Pacs008StructureValidator();
        return structureValidator.validate(xml);
    }

    private RejectionMotives validateValues(Pacs008 pacs008, ValuesValidator validator) {
        return validator.validate(pacs008);
    }

    private Pacs008 parse(String xml, Parser parser) {
        xml = xmlHandler(xml);
        return parser.parse(xml);
    }

    private String xmlHandler(String xml) {
        xml = xml.replace("xmlns", "Xmlns");
        return xml;
    }
}
