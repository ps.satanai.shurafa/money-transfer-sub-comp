package com.progressoft;

import javax.xml.bind.annotation.XmlElement;

public class ClientInformation {
    private String name;
    private PostalAddress postalAddress;

    @XmlElement(name="Nm")
    public void setName(String name) {
        this.name = name;
    }

    @XmlElement(name="PstlAdr")
    public void setPostalAddress(PostalAddress postalAddress) {
        this.postalAddress = postalAddress;
    }

    public String getName() {
        return name;
    }

    public PostalAddress getPostalAddress() {
        return postalAddress;
    }
}
