package com.progressoft;

import javax.xml.bind.annotation.XmlElement;

public class OriginalAgent {
    private FinancialInstitutionIdentification finInstnId;

    @XmlElement(name="FinInstnId")
    public void setFinInstnId(FinancialInstitutionIdentification finInstnId) {
        this.finInstnId = finInstnId;
    }

    public FinancialInstitutionIdentification getFinInstnId() {
        return finInstnId;
    }
}
