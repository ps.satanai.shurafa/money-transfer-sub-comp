package com.progressoft.jip;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Random;

public class FileReaderTest {
    @Test
    public void can_create() {
        new FileReader();
    }

    @Test
    public void givenNull_whenRead_thenFail() {
        FileReader reader = new FileReader();
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> reader.read(null));
        Assertions.assertEquals("File cannot be null", exception.getMessage());
    }

    @Test
    public void givenNotExistFile_whenRead_thenFail() {
        FileReader reader = new FileReader();
        File file = new File(".", "dir" + new Random().nextInt());
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> reader.read(file));
        Assertions.assertEquals("File does not exist", exception.getMessage());
    }

    @Test
    public void givenDirectory_whenRead_thenFail() throws IOException {
        FileReader reader = new FileReader();
        Path path = Files.createTempDirectory("bis-dir");
        File file = new File(String.valueOf(path));
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> reader.read(file));
        Assertions.assertEquals("Cannot be a directory", exception.getMessage());
    }

    @Test
    public void givenValidFile_whenRead_ThenSuccess() {
        File file = new File("src/test/resources/reader-test.txt");
        FileReader reader = new FileReader();
        String result = reader.read(file);
        Assertions.assertEquals("\nHello", result);
    }

}
