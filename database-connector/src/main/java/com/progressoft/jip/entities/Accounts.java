package com.progressoft.jip.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "accounts")
public class Accounts {
    @Id
    @Column(
            name = "IBAN",
            columnDefinition = "TEXT",
            unique = true
    )
    private String IBAN;

    @Column(
            name = "BIC",
            columnDefinition = "TEXT"
    )
    private String BIC;

    public String getIBAN() {
        return IBAN;
    }

    public String getBIC() {
        return BIC;
    }

    public void setIBAN(String IBAN) {
        this.IBAN = IBAN;
    }

    public void setBIC(String BIC) {
        this.BIC = BIC;
    }
}
