package com.progressoft;

import javax.xml.bind.annotation.XmlElement;

public class SettlementInformation {
    private String settlementMethod;

    @XmlElement(name = "SttlmMtd")
    public void setSettlementMethod(String settlementMethod) {
        this.settlementMethod = settlementMethod;
    }

    public String getSettlementMethod() {
        return settlementMethod;
    }
}
