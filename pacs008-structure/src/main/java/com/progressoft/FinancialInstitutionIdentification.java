package com.progressoft;

import javax.xml.bind.annotation.XmlElement;

public class FinancialInstitutionIdentification {
    private String BICFI;

    @XmlElement(name = "BICFI")
    public void setBICFI(String BICFI) {
        this.BICFI = BICFI;
    }

    public String getBICFI() {
        return BICFI;
    }
}
