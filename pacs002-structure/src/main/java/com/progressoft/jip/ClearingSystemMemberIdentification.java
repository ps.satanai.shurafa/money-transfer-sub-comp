package com.progressoft.jip;

import javax.xml.bind.annotation.XmlElement;

public class ClearingSystemMemberIdentification {
    private String memberIdentification;

    @XmlElement(name = "MmbId")
    public void setMemberIdentification(String memberIdentification) {
        this.memberIdentification = memberIdentification;
    }

    public String getMemberIdentification() {
        return memberIdentification;
    }
}
