package com.progressoft.jip;

import com.progressoft.*;
import com.progressoft.GroupHeader;
import com.progressoft.InstructingAgent;


import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.util.ArrayList;

public class Pacs008Parser implements Parser {
    @Override
    public Pacs008 parse(String pacs8Xml) {
        if (isNull(pacs8Xml))
            throw new NullPointerException("The XML cannot be null");

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Pacs8Doc.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            Pacs8Doc pacs8Doc = (Pacs8Doc) unmarshaller.unmarshal(new StringReader(pacs8Xml));
            return convertDocIntoPojo(pacs8Doc);
        } catch (JAXBException e) {
            System.out.println(e.getMessage() + " inside parser");
        }
        return null;
    }

    private Pacs008 convertDocIntoPojo(Pacs8Doc pacs8Doc) {
        Pacs008 pacs008 = new Pacs008();
        setGroupHeader(pacs8Doc, pacs008);
        setCreditTransfTransInfo(pacs8Doc, pacs008);
        setPaymentIdentification(pacs8Doc, pacs008);
        setPaymentTypeInfo(pacs8Doc, pacs008);
        setCurrencyAndAmount(pacs8Doc, pacs008);
        setInstructedAndInstructingInfo(pacs8Doc, pacs008);
        setDebtorInfo(pacs8Doc, pacs008);
        setCreditorInfo(pacs8Doc, pacs008);

        return pacs008;
    }

    private void setCreditorInfo(Pacs8Doc pacs8Doc, Pacs008 pacs008) {
        CreditTransferTransactionInformation creditTransferTransactionInformation = pacs8Doc.getfIToFICstmrCdtTrf().getCdtTrfTxInf();
        ClientInformation creditorInfo = creditTransferTransactionInformation.getCreditorInfo();
        Account creditorAccount = creditTransferTransactionInformation.getCreditorAccount();
        OriginalAgent creditorAgent = creditTransferTransactionInformation.getCreditorAgent();

        pacs008.setCreditorName(creditorInfo.getName());
        pacs008.setCreditorAddress(getAddress(creditorInfo));
        pacs008.setCreditorIBAN(creditorAccount.getAccountId().getIBAN());
        pacs008.setCreditorBICFI(creditorAgent.getFinInstnId().getBICFI());
    }

    private void setDebtorInfo(Pacs8Doc pacs8Doc, Pacs008 pacs008) {
        ClientInformation debtorInfo = pacs8Doc.getfIToFICstmrCdtTrf().getCdtTrfTxInf().getDebtorInfo();
        Account debtorAccount = pacs8Doc.getfIToFICstmrCdtTrf().getCdtTrfTxInf().getDebtorAccount();
        OriginalAgent debtorAgent = pacs8Doc.getfIToFICstmrCdtTrf().getCdtTrfTxInf().getDebtorAgent();
        pacs008.setDebtorName(debtorInfo.getName());
        pacs008.setDebtorAddress(getAddress(debtorInfo));
        pacs008.setDebtorIBAN(debtorAccount.getAccountId().getIBAN());
        pacs008.setDebtorBICFI(debtorAgent.getFinInstnId().getBICFI());
    }

    private String getAddress(ClientInformation clientInformation) {
        StringBuilder address = new StringBuilder();
        ArrayList<String> addressLines = clientInformation.getPostalAddress().getAdrLine();

        for (String addressLine : addressLines) address.append(addressLine).append(" ");

        return address.toString();
    }

    private void setInstructedAndInstructingInfo(Pacs8Doc pacs8Doc, Pacs008 pacs008) {
        InstructingAgent instructingAgent = pacs8Doc.getfIToFICstmrCdtTrf().getCdtTrfTxInf().getInstructingAgent();
        InstructingAgent instructedAgent = pacs8Doc.getfIToFICstmrCdtTrf().getCdtTrfTxInf().getInstructedAgent();

        pacs008.setInstructingBICFI(instructingAgent.getFinInstnId().getBICFI());
        pacs008.setInstructedBICFI(instructedAgent.getFinInstnId().getBICFI());
    }

    private void setCreditTransfTransInfo(Pacs8Doc pacs8Doc, Pacs008 pacs008) {
        CreditTransferTransactionInformation creditTransferTransactionInformation = pacs8Doc.getfIToFICstmrCdtTrf().getCdtTrfTxInf();
        pacs008.setIntrBkSttlmDt(creditTransferTransactionInformation.getIntrBkSttlmDt());
        pacs008.setChargeBearer(creditTransferTransactionInformation.getChargeBearer());
    }

    private void setCurrencyAndAmount(Pacs8Doc pacs8Doc, Pacs008 pacs008) {
        CreditTransferTransactionInformation creditTransferTransactionInformation = pacs8Doc.getfIToFICstmrCdtTrf().getCdtTrfTxInf();
        pacs008.setCurrency(creditTransferTransactionInformation.getIntrBkSttlmAmt().getCurrency());
        pacs008.setAmount(creditTransferTransactionInformation.getIntrBkSttlmAmt().getAmount());
    }

    private void setPaymentTypeInfo(Pacs8Doc pacs8Doc, Pacs008 pacs008) {
        PaymentTypeInformation paymentTypeInformation = pacs8Doc.getfIToFICstmrCdtTrf().getCdtTrfTxInf().getPmtTpInf();
        pacs008.setClrChanl(paymentTypeInformation.getClrChanl());
        pacs008.setServiceLevel(paymentTypeInformation.getServiceLevel().getPrtry());
        pacs008.setCategoryPurpose(paymentTypeInformation.getCategoryPurpose().getPrtry());
        pacs008.setLocalInstrument(paymentTypeInformation.getLocalInstrument().getPrtry());
    }

    private void setPaymentIdentification(Pacs8Doc pacs8Doc, Pacs008 pacs008) {
        PaymentIdentification paymentIdentification = pacs8Doc.getfIToFICstmrCdtTrf().getCdtTrfTxInf().getPmtId();
        pacs008.setInstrId(paymentIdentification.getInstrId());
        pacs008.setEndToEndId(paymentIdentification.getEndToEndId());
        pacs008.setTxtId(paymentIdentification.getTxId());
    }

    private void setGroupHeader(Pacs8Doc pacs8Doc, Pacs008 pacs008) {
        GroupHeader groupHeader = pacs8Doc.getfIToFICstmrCdtTrf().getGroupHeader();
        pacs008.setMessageID(groupHeader.getMessageId());
        pacs008.setCreationDateTime(groupHeader.getCreationDateTime());
        pacs008.setNumberOfTransactions(groupHeader.getNumberOfTransactions());
        pacs008.setSettlementMethod(groupHeader.getSettlementInformation().getSettlementMethod());
    }

    private boolean isNull(String pacs008) {
        return pacs008 == null;
    }

}
