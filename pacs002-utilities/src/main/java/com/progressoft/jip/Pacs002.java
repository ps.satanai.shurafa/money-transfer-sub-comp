package com.progressoft.jip;

public class Pacs002 {
    private String messageId;
    private String originalCreationDateTime;
    private String instructingAgentBICFI;
    private String instructedMemberIdentification;
    private String originalMessageIdentification;
    private String originalMessageNameIdentification;
    private String responseType;
    private String originalInstructionIdentification;
    private String originalEndToEndIdentification;
    private String originalTransactionIdentification;
    private String interbankSettlementDate;
    private String currency;
    private String amount;
    private String rejectionReson;
    private String rejectionAdditionalInfo;
    private String instructgAgentBICFI;
    private String instructdAgentBICFI;


    public Pacs002() {
        rejectionReson = null;
        rejectionAdditionalInfo = null;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public void setOriginalCreationDateTime(String originalCreationDateTime) {
        this.originalCreationDateTime = originalCreationDateTime;
    }

    public void setInstructingAgentBICFI(String instructingAgentBICFI) {
        this.instructingAgentBICFI = instructingAgentBICFI;
    }

    public void setInstructedMemberIdentification(String instructedMemberIdentification) {
        this.instructedMemberIdentification = instructedMemberIdentification;
    }

    public void setOriginalMessageIdentification(String originalMessageIdentification) {
        this.originalMessageIdentification = originalMessageIdentification;
    }

    public void setOriginalMessageNameIdentification(String originalMessageNameIdentification) {
        this.originalMessageNameIdentification = originalMessageNameIdentification;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public void setOriginalInstructionIdentification(String originalInstructionIdentification) {
        this.originalInstructionIdentification = originalInstructionIdentification;
    }

    public void setOriginalEndToEndIdentification(String originalEndToEndIdentification) {
        this.originalEndToEndIdentification = originalEndToEndIdentification;
    }

    public void setOriginalTransactionIdentification(String originalTransactionIdentification) {
        this.originalTransactionIdentification = originalTransactionIdentification;
    }

    public void setInterbankSettlementDate(String interbankSettlementDate) {
        this.interbankSettlementDate = interbankSettlementDate;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void setRejectionReson(String rejectionReson) {
        this.rejectionReson = rejectionReson;
    }

    public void setRejectionAdditionalInfo(String rejectionAdditionalInfo) {
        this.rejectionAdditionalInfo = rejectionAdditionalInfo;
    }

    public void setInstructgAgentBICFI(String instructgAgentBICFI) {
        this.instructgAgentBICFI = instructgAgentBICFI;
    }

    public void setInstructdAgentBICFI(String instructdAgentBICFI) {
        this.instructdAgentBICFI = instructdAgentBICFI;
    }

    public String getMessageId() {
        return messageId;
    }

    public String getOriginalCreationDateTime() {
        return originalCreationDateTime;
    }

    public String getInstructingAgentBICFI() {
        return instructingAgentBICFI;
    }

    public String getInstructedMemberIdentification() {
        return instructedMemberIdentification;
    }

    public String getOriginalMessageIdentification() {
        return originalMessageIdentification;
    }

    public String getOriginalMessageNameIdentification() {
        return originalMessageNameIdentification;
    }

    public String getResponseType() {
        return responseType;
    }

    public String getOriginalInstructionIdentification() {
        return originalInstructionIdentification;
    }

    public String getOriginalEndToEndIdentification() {
        return originalEndToEndIdentification;
    }

    public String getOriginalTransactionIdentification() {
        return originalTransactionIdentification;
    }

    public String getInterbankSettlementDate() {
        return interbankSettlementDate;
    }

    public String getCurrency() {
        return currency;
    }

    public String getAmount() {
        return amount;
    }

    public String getInstructgAgentBICFI() {
        return instructgAgentBICFI;
    }

    public String getInstructdAgentBICFI() {
        return instructdAgentBICFI;
    }

    public String getRejectionReson() {
        return rejectionReson;
    }

    public String getRejectionAdditionalInfo() {
        return rejectionAdditionalInfo;
    }

}
