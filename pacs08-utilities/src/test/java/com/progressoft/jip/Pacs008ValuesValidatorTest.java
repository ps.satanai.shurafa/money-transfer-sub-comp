package com.progressoft.jip;

import com.progressoft.jip.entities.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDate;

public class Pacs008ValuesValidatorTest {
    private Pacs008 pacs008;
    private MessagesRepository messagesRepository;
    private AccountsRepository accountsRepository;
    private ParticipantsRepository participantsRepository;
    private CurrenciesRepository currenciesRepository;
    private Pacs008ValuesValidator validator;

    @BeforeEach
    public void setUp() {
        pacs008 = new Pacs008();
        initEach();

        messagesRepository = mock(MessagesRepository.class);
        accountsRepository = mock(AccountsRepository.class);
        participantsRepository = mock(ParticipantsRepository.class);
        PurposeCodesRepository purposeCodesRepository = mock(PurposeCodesRepository.class);
        currenciesRepository = mock(CurrenciesRepository.class);

        validator = new Pacs008ValuesValidator(messagesRepository, participantsRepository, accountsRepository,
                currenciesRepository, purposeCodesRepository);

        when(participantsRepository.existsById("HBHOJOA0")).thenReturn(true);
        when(participantsRepository.existsById("JGBAJOA0")).thenReturn(true);
        when(accountsRepository.existsById("JO93JGBA6010000290450010010000")).thenReturn(true);
        when(accountsRepository.existsById("JO83HBHO0320000033330600101001")).thenReturn(true);
    }

    @Test
    public void givenPacs8WithInvalidSettlementDate_wheValidate_thenTheRsnShouldBeFF03() {
        pacs008.setIntrBkSttlmDt("2020-01-06");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenPacs8WithInvalidCreditorName_whenValidate_thenTheRsnShouldBeFF03() {
        pacs008.setCreditorName("AHMAD SALEH");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenPacs8WithInvalidDebtorName_whenValidate_thenTheRsnShouldBeFF03() {
        pacs008.setDebtorName("AHMAD SALEH  ");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenPacs8WithDebtorAddressLessThan2Parts_whenValidate_thenTheRsnShouldBeFF03() {
        pacs008.setDebtorAddress("Amman");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenPacs8WithCreditorAddressLessThan2Parts_whenValidate_thenTheRsnShouldBeFF03() {
        pacs008.setCreditorAddress(" Amman");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenPacs8WithInvalidCreditorAddress_whenValidate_thenTheRsnShouldBeFF03() {
        pacs008.setCreditorAddress("Amm234an Jordan");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenPacs8WithInvalidDebtorAddress_whenValidate_thenTheRsnShouldBeFF03() {
        pacs008.setDebtorAddress("12@mm amm");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenPacs8WithInvalidCreditorIBAN_whenValidate_thenTheRsnShouldBeAC03() {
        pacs008.setCreditorIBAN("JO012345678900");
        Assertions.assertEquals(RejectionMotives.AC03, validator.validate(pacs008));
    }

    @Test
    public void givenPacs8WithInvalidDebtorIBAN_whenValidate_thenTheRsnShouldBeAC02() {
        pacs008.setDebtorIBAN("1102940983403");
        Assertions.assertEquals(RejectionMotives.AC02, validator.validate(pacs008));
    }

    @Test
    public void givenPacs8WithInvalidMsgID_whenValidate_thenTheRsnShouldBeFF03() {
        pacs008.setMessageID("JO345678909876543212345");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenPacs8WithInvalidInstrID_whenValidate_thenTheRsnShouldBeFF03() {
        pacs008.setInstrId("JO345678909543212345");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenPacs8WithInvalidTxID_whenValidate_thenTheRsnShouldBeFF03() {
        pacs008.setTxtId("JODC8909876543212345");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenPacs8WithInvalidInstructedAgentBIC_whenValidate_thenTheRsnShouldBeBE20() {
        pacs008.setInstructedBICFI("ABCD");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenPacs8WithInvalidInstructingAgentBIC_whenValidate_thenTheRsnShouldBeBE20() {
        pacs008.setInstructingBICFI("ABCDAJ0");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenPacs8WithInvalidDebtorBIC_whenValidate_thenTheRsnShouldBeBE20() {
        pacs008.setDebtorBICFI("ABCD JOA0");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenPacs8WithInvalidCreditorBIC_whenValidate_thenTheRsnShouldBeBE20() {
        pacs008.setCreditorBICFI("ABCDOA0");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenPacs8WithDifferentDebtorBICAndInstructingBIC_whenValidate_thenTheRsnShouldBeFF03() {
        pacs008.setDebtorBICFI("NBOKJOA0");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenPacs8WithDifferentCreditorBICAndInstructedBIC_whenValidate_thenTheRsnShouldBeFF03() {
        pacs008.setCreditorBICFI("NBOKJOA0");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenInvalidPurposeCode_whenValidate_thenTheRsnShouldBeFF03() {
        pacs008.setCategoryPurpose("A1110");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenInvalidCurrency_whenValidate_thenTheRsnShouldBeFF03() {
        pacs008.setCurrency("JOD12");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenNotExistBICForInstructingAgent_whenValidate_thenTheRsnShouldBeFF03() {
        String BIC = "AAAAJOA0";
        pacs008.setInstructingBICFI(BIC);
        when(participantsRepository.existsById(BIC)).thenReturn(false);
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenNotExistBICForInstructedAgent_whenValidate_thenTheRsnShouldBeFF03() {
        String BIC = "ABABJOA0";
        pacs008.setInstructedBICFI(BIC);
        when(participantsRepository.existsById(BIC)).thenReturn(false);
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenNotExistIBANForDebtor_whenValidate_thenTheRsnShouldBeAC02() {
        String IBAN = "JO93JGBA6010393290450010011111";
        pacs008.setDebtorIBAN(IBAN);
        Assertions.assertEquals(RejectionMotives.AC02, validator.validate(pacs008));
    }

    @Test
    public void givenNotExistIBANForCreditor_whenValidate_thenTheRsnShouldBeAC03() {
        String IBAN = "JO93JGBA6010393290450010011111";
        pacs008.setCreditorIBAN(IBAN);
        when(accountsRepository.existsById(IBAN)).thenReturn(false);
        Assertions.assertEquals(RejectionMotives.AC03, validator.validate(pacs008));
    }

    @Test
    public void givenExistMsgID_whenValidate_thenTheRsnShouldBeFF03() {
        String msgId = "JGBA2812094750123456789";
        pacs008.setMessageID(msgId);

        when(messagesRepository.existsById(msgId)).thenReturn(true);
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenNotExistPurposeCode_whenValidate_thenTheRsnShouldBeFF03() {
        String purposeCode = "00000";
        pacs008.setCategoryPurpose(purposeCode);
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenNotExistCurrency_whenValidate_thenTheRsnShouldBeFF03() {
        String currency = "SR";
        pacs008.setCurrency(currency);
        when(currenciesRepository.existsById(currency)).thenReturn(false);
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenAmountEqualsZero_whenValidate_thenTheRsnShouldBeFF03() {
        pacs008.setAmount("0");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenNegativeAmount_whenValidate_thenTheRsnShouldBeFF03() {
        pacs008.setAmount("-1000");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenInvalidAmountFormat_whenValidate_thenTheRsnShouldBeFF03() {
        pacs008.setAmount("-100");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenInvalidNumOfTxt_whenValidate_thenTheRsnShouldBeFF03() {
        pacs008.setNumberOfTransactions(3);
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenInvalidSttlmMttd_whenValidate_thenTheRsnShouldBeFF03() {
        pacs008.setSettlementMethod("ABCD");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenInvalidClrChnal_whenValidate_thenTheRsnShouldBeFF03() {
        pacs008.setClrChanl("A123");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenInvalidServiceLevel_whenValidate_thenTheRsnShouldBeFF03() {
        pacs008.setServiceLevel("11##");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenInvalidLclInstrm_whenValidate_thenTheRsnShouldBeFF03() {
        pacs008.setLocalInstrument("AAAA");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }

    @Test
    public void givenInvalidChrgBr_whenValidate_thenTheRsnShouldBeFF03() {
        pacs008.setChargeBearer("A123");
        Assertions.assertEquals(RejectionMotives.FF03, validator.validate(pacs008));
    }


    public void initEach() {
        LocalDate currentDate = java.time.LocalDate.now();

        pacs008.setMessageID("JGBA2800094750927004101");
        pacs008.setCreationDateTime("2020-12-28T07:47:50Z");
        pacs008.setNumberOfTransactions(1);
        pacs008.setSettlementMethod("CLRG");
        pacs008.setInstrId("JGBA2812094750927334298");
        pacs008.setEndToEndId("NOTPROVIDED");
        pacs008.setTxtId("JGBA2812094750927334297");
        pacs008.setClrChanl("RTNS");
        pacs008.setServiceLevel("0100");
        pacs008.setLocalInstrument("CSDC");
        pacs008.setCategoryPurpose("11110");
        pacs008.setIntrBkSttlmDt(currentDate.toString());
        pacs008.setCurrency("JOD");
        pacs008.setAmount("100");
        pacs008.setChargeBearer("SLEV");
        pacs008.setInstructingBICFI("JGBAJOA0");
        pacs008.setInstructedBICFI("HBHOJOA0");
        pacs008.setDebtorName("NISREEN MOHAMMAD YOUSEF");
        pacs008.setDebtorAddress("new zarqa");
        pacs008.setDebtorIBAN("JO93JGBA6010000290450010010000");
        pacs008.setDebtorBICFI("JGBAJOA0");
        pacs008.setCreditorBICFI("HBHOJOA0");
        pacs008.setCreditorName("MAEN SAMI HATTAR SALEM");
        pacs008.setCreditorAddress("Amman Jordan SLT JO SLT");
        pacs008.setCreditorIBAN("JO83HBHO0320000033330600101001");

    }

}
