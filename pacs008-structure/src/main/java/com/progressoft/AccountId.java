package com.progressoft;

import javax.xml.bind.annotation.XmlElement;

public class AccountId {
    private String IBAN;

    @XmlElement(name = "IBAN")
    public void setIBAN(String IBAN) {
        this.IBAN = IBAN;
    }

    public String getIBAN() {
        return IBAN;
    }
}
