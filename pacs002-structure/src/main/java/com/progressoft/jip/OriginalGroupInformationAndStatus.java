package com.progressoft.jip;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType( propOrder = {"originalMessageIdentification",
        "originalMessageNameIdentification",
        "stsRsnInf"
})
public class OriginalGroupInformationAndStatus {
    private String originalMessageIdentification;
    private String originalMessageNameIdentification;
    private StatusReasonInformation stsRsnInf;

    @XmlElement(name = "OrgnlMsgId")
    public void setOriginalMessageIdentification(String originalMessageIdentification) {
        this.originalMessageIdentification = originalMessageIdentification;
    }

    @XmlElement(name = "OrgnlMsgNmId")
    public void setOriginalMessageNameIdentification(String originalMessageNameIdentification) {
        this.originalMessageNameIdentification = originalMessageNameIdentification;
    }

    @XmlElement(name = "StsRsnInf")
    public void setStsRsnInf(StatusReasonInformation stsRsnInf) {
        this.stsRsnInf = stsRsnInf;
    }

    public String getOriginalMessageIdentification() {
        return originalMessageIdentification;
    }

    public String getOriginalMessageNameIdentification() {
        return originalMessageNameIdentification;
    }

    public StatusReasonInformation getStsRsnInf() {
        return stsRsnInf;
    }
}

