package com.progressoft;

import javax.xml.bind.annotation.XmlElement;

public class ServiceLevel {

    private String prtry;

    public void setPrtry(String prtry) {
        this.prtry = prtry;
    }
    @XmlElement(name = "Prtry")
    public String getPrtry() {
        return prtry;
    }
}
