package com.progressoft.jip;

public class Pacs008 {
    private String messageID;
    private String creationDateTime;
    private int numberOfTransactions;
    private String settlementMethod;
    private String EndToEndId;
    private String txtId;
    private String instrId;
    private String clrChanl;
    private String serviceLevel;
    private String localInstrument;
    private String categoryPurpose;
    private String currency;
    private String amount;
    private String intrBkSttlmDt;
    private String chargeBearer;
    private String instructingBICFI;
    private String instructedBICFI;
    private String debtorBICFI;
    private String creditorBICFI;
    private String debtorName;
    private String creditorName;
    private String creditorAddress;
    private String debtorAddress;
    private String debtorIBAN;
    private String creditorIBAN;


    public void setInstrId(String instrId) {
        this.instrId = instrId;
    }


    public void setMessageID(String messageID) {
        this.messageID = messageID;
    }

    public void setCreationDateTime(String creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    public void setNumberOfTransactions(int numberOfTransactions) {
        this.numberOfTransactions = numberOfTransactions;
    }

    public void setSettlementMethod(String settlementMethod) {
        this.settlementMethod = settlementMethod;
    }

    public void setEndToEndId(String endToEndId) {
        EndToEndId = endToEndId;
    }

    public void setTxtId(String txtId) {
        this.txtId = txtId;
    }

    public void setClrChanl(String clrChanl) {
        this.clrChanl = clrChanl;
    }

    public void setServiceLevel(String serviceLevel) {
        this.serviceLevel = serviceLevel;
    }

    public void setLocalInstrument(String localInstrument) {
        this.localInstrument = localInstrument;
    }

    public void setCategoryPurpose(String categoryPurpose) {
        this.categoryPurpose = categoryPurpose;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void setIntrBkSttlmDt(String intrBkSttlmDt) {
        this.intrBkSttlmDt = intrBkSttlmDt;
    }

    public void setChargeBearer(String chargeBearer) {
        this.chargeBearer = chargeBearer;
    }

    public void setInstructingBICFI(String instructingBICFI) {
        this.instructingBICFI = instructingBICFI;
    }

    public void setInstructedBICFI(String instructedBICFI) {
        this.instructedBICFI = instructedBICFI;
    }

    public void setDebtorBICFI(String debtorBICFI) {
        this.debtorBICFI = debtorBICFI;
    }

    public void setCreditorBICFI(String creditorBICFI) {
        this.creditorBICFI = creditorBICFI;
    }

    public void setDebtorName(String debtorName) {
        this.debtorName = debtorName;
    }

    public void setCreditorName(String creditorName) {
        this.creditorName = creditorName;
    }

    public void setCreditorAddress(String creditorAddress) {
        this.creditorAddress = creditorAddress;
    }

    public void setDebtorAddress(String debtorAddress) {
        this.debtorAddress = debtorAddress;
    }

    public void setDebtorIBAN(String debtorIBAN) {
        this.debtorIBAN = debtorIBAN;
    }

    public void setCreditorIBAN(String creditorIBAN) {
        this.creditorIBAN = creditorIBAN;
    }

    public String getMessageID() {
        return messageID;
    }

    public String getCreationDateTime() {
        return creationDateTime;
    }

    public int getNumberOfTransactions() {
        return numberOfTransactions;
    }

    public String getSettlementMethod() {
        return settlementMethod;
    }

    public String getEndToEndId() {
        return EndToEndId;
    }

    public String getTxtId() {
        return txtId;
    }

    public String getClrChanl() {
        return clrChanl;
    }

    public String getServiceLevel() {
        return serviceLevel;
    }

    public String getLocalInstrument() {
        return localInstrument;
    }

    public String getCategoryPurpose() {
        return categoryPurpose;
    }

    public String getInstrId() {
        return instrId;
    }

    public String getCurrency() {
        return currency;
    }

    public String getAmount() {
        return amount;
    }

    public String getIntrBkSttlmDt() {
        return intrBkSttlmDt;
    }

    public String getChargeBearer() {
        return chargeBearer;
    }

    public String getInstructingBICFI() {
        return instructingBICFI;
    }

    public String getInstructedBICFI() {
        return instructedBICFI;
    }

    public String getDebtorBICFI() {
        return debtorBICFI;
    }

    public String getCreditorBICFI() {
        return creditorBICFI;
    }

    public String getDebtorName() {
        return debtorName;
    }

    public String getCreditorName() {
        return creditorName;
    }

    public String getCreditorAddress() {
        return creditorAddress;
    }

    public String getDebtorAddress() {
        return debtorAddress;
    }

    public String getDebtorIBAN() {
        return debtorIBAN;
    }

    public String getCreditorIBAN() {
        return creditorIBAN;
    }


}
