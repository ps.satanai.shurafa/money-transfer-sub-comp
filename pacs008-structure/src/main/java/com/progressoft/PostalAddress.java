package com.progressoft;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;

public class PostalAddress {
    //private String adrLine;
    private ArrayList<String > adrLine;
    @XmlElement(name="AdrLine")
    public void setAdrLine(ArrayList<String> adrLine) {
        this.adrLine = adrLine;
    }

    public ArrayList<String> getAdrLine() {
        return adrLine;
    }

    //
    //public void setAdrLine(String adrLine) {
       // this.adrLine = adrLine;
    //}

   // public String getAdrLine() {
      //  return adrLine;
    ///}
}
