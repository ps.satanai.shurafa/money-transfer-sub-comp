package com.progressoft;

import javax.xml.bind.annotation.XmlElement;

public class PaymentTypeInformation {
    private String ClrChanl;
    private ServiceLevel serviceLevel;
    private LocalInstrument localInstrument;
    private CategoryPurpose categoryPurpose;

    @XmlElement(name = "ClrChanl")
    public void setClrChanl(String clrChanl) {
        ClrChanl = clrChanl;
    }

    @XmlElement(name = "SvcLvl")
    public void setServiceLevel(ServiceLevel serviceLevel) {
        this.serviceLevel = serviceLevel;
    }

    @XmlElement(name = "LclInstrm")
    public void setLocalInstrument(LocalInstrument localInstrument) {
        this.localInstrument = localInstrument;
    }

    @XmlElement(name = "CtgyPurp")
    public void setCategoryPurpose(CategoryPurpose categoryPurpose) {
        this.categoryPurpose = categoryPurpose;
    }

    public String getClrChanl() {
        return ClrChanl;
    }

    public ServiceLevel getServiceLevel() {
        return serviceLevel;
    }

    public LocalInstrument getLocalInstrument() {
        return localInstrument;
    }

    public CategoryPurpose getCategoryPurpose() {
        return categoryPurpose;
    }
}
