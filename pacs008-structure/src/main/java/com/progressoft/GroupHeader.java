package com.progressoft;

import javax.xml.bind.annotation.XmlElement;

public class GroupHeader {
    public GroupHeader(){
        messageId = null;
    }
    private String messageId;
    private String creationDateTime;
    private int numberOfTransactions;
    private SettlementInformation settlementInformation;

    @XmlElement(name = "MsgId")
    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    @XmlElement(name = "CreDtTm")
    public void setCreationDateTime(String creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    @XmlElement(name = "NbOfTxs")
    public void setNumberOfTransactions(int numberOfTransactions) {
        this.numberOfTransactions = numberOfTransactions;
    }

    @XmlElement(name = "SttlmInf")
    public void setSettlementInformation(SettlementInformation settlementInformation) {
        this.settlementInformation = settlementInformation;
    }

    public String getMessageId() {
        return messageId;
    }

    public String getCreationDateTime() {
        return creationDateTime;
    }

    public int getNumberOfTransactions() {
        return numberOfTransactions;
    }

    public SettlementInformation getSettlementInformation() {
        return settlementInformation;
    }
}
