package com.progressoft.jip;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

@RestController
public class PacsController {
    private final Pacs008Handler handler;

    public PacsController(Pacs008Handler handler) {
        this.handler = handler;
    }

    @RequestMapping(value = "/response", produces = "application/xml")
    public String getResponse(@RequestParam(name = "pacs") String pacs) {
        String response;
        if (StringUtils.isBlank(pacs))
            response = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                    "<Error>THE MESSAGE CANNOT BE EMPTY!!!</Error>";
        else {
            ResponseData responseData = handler.handle(pacs);
            if (isNull(responseData.getXml()))
                response = responseData.getDesc();
            else
                response = responseData.getXml();
        }
        return response;
    }

    private boolean isNull(String xml) {
        return xml == null;
    }

}
