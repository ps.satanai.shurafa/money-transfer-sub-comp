package com.progressoft.jip;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;

public class Pacs002Serializer implements Serializer {
    private final Pacs002Doc pacs002Doc;

    public Pacs002Serializer() {
        pacs002Doc = new Pacs002Doc();
    }

    @Override
    public String serialize(Pacs002 pacs002Pojo) {
        convertPojoIntoDoc(pacs002Pojo);
        try {
            StringWriter writer = new StringWriter();
            JAXBContext jaxbContext = JAXBContext.newInstance(Pacs002Doc.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.marshal(pacs002Doc, writer);
            return writer.toString();
        } catch (JAXBException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private void convertPojoIntoDoc(Pacs002 pacs002Pojo) {
        setGroupHeader(pacs002Pojo);
        setOriginalGroupInformationAndStatus(pacs002Pojo);
        setTransactionInformationAndStatus(pacs002Pojo);
    }

    private void setTransactionInformationAndStatus(Pacs002 pacs002Pojo) {
        TransactionInformationAndStatus transactionInformationAndStatus = new TransactionInformationAndStatus();
        InstructedAgent instructedAgent = new InstructedAgent();
        InstructingAgent instructingAgent = new InstructingAgent();
        FinancialInstitutionIdentification financialInstitutionIdentification = new FinancialInstitutionIdentification();
        FinancialInstitutionIdentification financialInstitutionIdentification1 = new FinancialInstitutionIdentification();
        OriginalTransactionReference originalTransactionReference = new OriginalTransactionReference();
        InterbankSettlementAmount interbankSettlementAmount = new InterbankSettlementAmount();

        Pacs002Types type = Pacs002Types.valueOf(pacs002Pojo.getResponseType());
        if (isNautPacs2(type))
            addRejectionMotive(transactionInformationAndStatus, pacs002Pojo);

        setOriginalTransactionReference(pacs002Pojo, transactionInformationAndStatus, originalTransactionReference, interbankSettlementAmount);
        setInstructingAgent(pacs002Pojo, transactionInformationAndStatus, instructingAgent, financialInstitutionIdentification);
        setInstructedAgent(pacs002Pojo, transactionInformationAndStatus, instructedAgent, financialInstitutionIdentification1);

        transactionInformationAndStatus.setOriginalTransactionIdentification(pacs002Pojo.getOriginalTransactionIdentification());
        transactionInformationAndStatus.setOriginalEndToEndIdentification(pacs002Pojo.getOriginalEndToEndIdentification());
        transactionInformationAndStatus.setOriginalInstructionIdentification(pacs002Pojo.getOriginalInstructionIdentification());

        pacs002Doc.getFiToFIPmtStsRpt().setTxInfAndSts(transactionInformationAndStatus);
    }

    private void setInstructedAgent(Pacs002 pacs002Pojo, TransactionInformationAndStatus transactionInformationAndStatus, InstructedAgent instructedAgent, FinancialInstitutionIdentification financialInstitutionIdentification1) {
        financialInstitutionIdentification1.setBICFI(pacs002Pojo.getInstructdAgentBICFI());
        instructedAgent.setFinInstnId(financialInstitutionIdentification1);
        transactionInformationAndStatus.setInstdAgt(instructedAgent);
    }

    private void setInstructingAgent(Pacs002 pacs002Pojo, TransactionInformationAndStatus transactionInformationAndStatus, InstructingAgent instructingAgent, FinancialInstitutionIdentification financialInstitutionIdentification) {
        financialInstitutionIdentification.setBICFI(pacs002Pojo.getInstructgAgentBICFI());
        instructingAgent.setFinInstnId(financialInstitutionIdentification);
        transactionInformationAndStatus.setInstgAgt(instructingAgent);
    }

    private void setOriginalTransactionReference(Pacs002 pacs002Pojo, TransactionInformationAndStatus transactionInformationAndStatus, OriginalTransactionReference originalTransactionReference, InterbankSettlementAmount interbankSettlementAmount) {
        interbankSettlementAmount.setAmount(pacs002Pojo.getAmount());
        interbankSettlementAmount.setCurrency(pacs002Pojo.getCurrency());
        originalTransactionReference.setIntrBkSttlmAmt(interbankSettlementAmount);
        originalTransactionReference.setInterbankSettlementDate(pacs002Pojo.getInterbankSettlementDate());
        transactionInformationAndStatus.setOrgnlTxRef(originalTransactionReference);
    }

    private void addRejectionMotive(TransactionInformationAndStatus transactionInformationAndStatus, Pacs002 pacs002Pojo) {
        StatusReasonInformation statusReasonInformation = new StatusReasonInformation();
        Reson reson = new Reson();
        reson.setPrtry(pacs002Pojo.getRejectionReson());
        statusReasonInformation.setRsn(reson);
        statusReasonInformation.setAdditionalInformation(pacs002Pojo.getRejectionAdditionalInfo());
        transactionInformationAndStatus.setStsRsnInf(statusReasonInformation);
    }

    private boolean isNautPacs2(Pacs002Types type) {
        return type.equals(Pacs002Types.NAUT);
    }

    private void setOriginalGroupInformationAndStatus(Pacs002 pacs002Pojo) {
        OriginalGroupInformationAndStatus originalGroupInformationAndStatus = new OriginalGroupInformationAndStatus();
        StatusReasonInformation statusReasonInformation = new StatusReasonInformation();
        Reson reson = new Reson();

        reson.setPrtry(pacs002Pojo.getResponseType());
        statusReasonInformation.setRsn(reson);
        originalGroupInformationAndStatus.setStsRsnInf(statusReasonInformation);
        originalGroupInformationAndStatus.setOriginalMessageIdentification(pacs002Pojo.getOriginalMessageIdentification());
        originalGroupInformationAndStatus.setOriginalMessageNameIdentification(pacs002Pojo.getOriginalMessageNameIdentification());

        pacs002Doc.getFiToFIPmtStsRpt().setOrgnlGrpInfAndSts(originalGroupInformationAndStatus);
    }

    private void setGroupHeader(Pacs002 pacs002Pojo) {
        FIToFIPmtStsRpt fiToFIPmtStsRpt = new FIToFIPmtStsRpt();
        GroupHeader groupHeader = new GroupHeader();
        InstructingAgent instructingAgent = new InstructingAgent();
        InstructedAgent instructedAgent = new InstructedAgent();
        FinancialInstitutionIdentification financialInstitutionIdentification = new FinancialInstitutionIdentification();
        FinancialInstitutionIdentification financialInstitutionIdentification1 = new FinancialInstitutionIdentification();
        ClearingSystemMemberIdentification clearingSystemMemberIdentification = new ClearingSystemMemberIdentification();

        clearingSystemMemberIdentification.setMemberIdentification(pacs002Pojo.getInstructedMemberIdentification());
        financialInstitutionIdentification.setClrSysMmbId(clearingSystemMemberIdentification);
        instructedAgent.setFinInstnId(financialInstitutionIdentification);
        groupHeader.setInstructedAgent(instructedAgent);

        financialInstitutionIdentification1.setBICFI(pacs002Pojo.getInstructingAgentBICFI());
        instructingAgent.setFinInstnId(financialInstitutionIdentification1);
        groupHeader.setInstructingAgent(instructingAgent);

        groupHeader.setMessageId(pacs002Pojo.getMessageId());
        groupHeader.setOriginalCreationDateTime(pacs002Pojo.getOriginalCreationDateTime());

        pacs002Doc.setFiToFIPmtStsRpt(fiToFIPmtStsRpt);
        pacs002Doc.getFiToFIPmtStsRpt().setGroupHeader(groupHeader);
    }
}
