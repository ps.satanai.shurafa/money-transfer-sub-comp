package com.progressoft.jip;

import java.io.File;
import java.io.IOException;

public class FileWriter {
    public void write(File file, String xml) {
        validateAgrs(file, xml);

        try (java.io.FileWriter writer = new java.io.FileWriter(file)) {
            writer.write(xml);
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    private void validateAgrs(File file, String xml) {
        if(file == null)
            throw new NullPointerException("File cannot be null");
        if(xml == null)
            throw new NullPointerException("Xml cannot be null");
    }
}
