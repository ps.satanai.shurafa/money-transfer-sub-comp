package com.progressoft;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

public class InterbankSettlementAmount {
    private String currency;
    private String amount;

    @XmlAttribute(name = "Ccy")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @XmlValue
    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getAmount() {
        return amount;
    }
}
