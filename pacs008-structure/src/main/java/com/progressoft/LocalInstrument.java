package com.progressoft;

import javax.xml.bind.annotation.XmlElement;

public class LocalInstrument {
    private String prtry;
    @XmlElement(name = "Prtry")
    public void setPrtry(String prtry) {
        this.prtry = prtry;
    }

    public String getPrtry() {
        return prtry;
    }
}
