package com.progressoft.jip;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType( propOrder = {"groupHeader",
        "orgnlGrpInfAndSts",
        "txInfAndSts"
})
public class FIToFIPmtStsRpt {
    private GroupHeader groupHeader;
    private OriginalGroupInformationAndStatus orgnlGrpInfAndSts;
    private TransactionInformationAndStatus txInfAndSts;

    @XmlElement(name = "GrpHdr")
    public void setGroupHeader(GroupHeader groupHeader) {
        this.groupHeader = groupHeader;
    }

    @XmlElement(name = "OrgnlGrpInfAndSts")
    public void setOrgnlGrpInfAndSts(OriginalGroupInformationAndStatus orgnlGrpInfAndSts) {
        this.orgnlGrpInfAndSts = orgnlGrpInfAndSts;
    }

    @XmlElement(name = "TxInfAndSts")
    public void setTxInfAndSts(TransactionInformationAndStatus txInfAndSts) {
        this.txInfAndSts = txInfAndSts;
    }

    public GroupHeader getGroupHeader() {
        return groupHeader;
    }

    public OriginalGroupInformationAndStatus getOrgnlGrpInfAndSts() {
        return orgnlGrpInfAndSts;
    }

    public TransactionInformationAndStatus getTxInfAndSts() {
        return txInfAndSts;
    }
}
