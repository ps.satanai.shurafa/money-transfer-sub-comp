package com.progressoft;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {
        "pmtId",
        "pmtTpInf",
        "chargeBearer",
        "intrBkSttlmAmt",
        "intrBkSttlmDt",
        "instructingAgent",
        "instructedAgent",
        "debtorInfo",
        "debtorAccount",
        "debtorAgent",
        "creditorAgent",
        "creditorInfo",
        "creditorAccount"
})
public class CreditTransferTransactionInformation {
    private PaymentIdentification pmtId;
    private PaymentTypeInformation pmtTpInf;
    private InterbankSettlementAmount IntrBkSttlmAmt;
    private String intrBkSttlmDt;
    private String chargeBearer;
    private InstructingAgent instructingAgent;
    private InstructingAgent instructedAgent;
    private OriginalAgent debtorAgent;
    private OriginalAgent creditorAgent;
    private ClientInformation debtorInfo;
    private Account debtorAccount;
    private Account creditorAccount;
    private ClientInformation creditorInfo;

    public OriginalAgent getDebtorAgent() {
        return debtorAgent;
    }
    public OriginalAgent getCreditorAgent() {
        return creditorAgent;
    }
    public PaymentIdentification getPmtId() {
        return pmtId;
    }

    public PaymentTypeInformation getPmtTpInf() {
        return pmtTpInf;
    }

    public InterbankSettlementAmount getIntrBkSttlmAmt() {
        return IntrBkSttlmAmt;
    }

    public String getIntrBkSttlmDt() {
        return intrBkSttlmDt;
    }

    public String getChargeBearer() {
        return chargeBearer;
    }

    public InstructingAgent getInstructingAgent() {
        return instructingAgent;
    }

    public InstructingAgent getInstructedAgent() {
        return instructedAgent;
    }

    public ClientInformation getDebtorInfo() {
        return debtorInfo;
    }

    public Account getDebtorAccount() {
        return debtorAccount;
    }

    public Account getCreditorAccount() {
        return creditorAccount;
    }

    public ClientInformation getCreditorInfo() {
        return creditorInfo;
    }


    @XmlElement(name = "PmtId")
    public void setPmtId(PaymentIdentification pmtId) {
        this.pmtId = pmtId;
    }

    @XmlElement(name = "PmtTpInf")
    public void setPmtTpInf(PaymentTypeInformation pmtTpInf) {
        this.pmtTpInf = pmtTpInf;
    }

    @XmlElement(name = "IntrBkSttlmAmt")
    public void setIntrBkSttlmAmt(InterbankSettlementAmount intrBkSttlmAmt) {
        IntrBkSttlmAmt = intrBkSttlmAmt;
    }

    @XmlElement(name = "IntrBkSttlmDt")
    public void setIntrBkSttlmDt(String intrBkSttlmDt) {
        this.intrBkSttlmDt = intrBkSttlmDt;
    }

    @XmlElement(name = "ChrgBr")
    public void setChargeBearer(String chargeBearer) {
        this.chargeBearer = chargeBearer;
    }

    @XmlElement(name = "InstgAgt")
    public void setInstructingAgent(InstructingAgent instructingAgent) {
        this.instructingAgent = instructingAgent;
    }

    @XmlElement(name = "InstdAgt")
    public void setInstructedAgent(InstructingAgent instructedAgent) {
        this.instructedAgent = instructedAgent;
    }

    @XmlElement(name = "DbtrAgt")
    public void setDebtorAgent(OriginalAgent debtorAgent) {
        this.debtorAgent = debtorAgent;
    }
    @XmlElement(name = "CdtrAgt")
    public void setCreditorAgent(OriginalAgent creditorAgent) {
        this.creditorAgent = creditorAgent;
    }

    @XmlElement(name = "Dbtr")
    public void setDebtorInfo(ClientInformation debtorInfo) {
        this.debtorInfo = debtorInfo;
    }

    @XmlElement(name = "DbtrAcct")
    public void setDebtorAccount(Account debtorAccount) {
        this.debtorAccount = debtorAccount;
    }

    @XmlElement(name = "CdtrAcct")
    public void setCreditorAccount(Account creditorAccount) {
        this.creditorAccount = creditorAccount;
    }

    @XmlElement(name = "Cdtr")
    public void setCreditorInfo(ClientInformation creditorInfo) {
        this.creditorInfo = creditorInfo;
    }
}
