package com.progressoft.jip.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "participants")
public class Participants {
    @Id
    @Column(
            name = "BIC",
            columnDefinition = "TEXT",
            unique = true
    )
    private String BIC;

    @Column(
            name = "Name",
            columnDefinition = "TEXT"
    )
    private String name;

    public String getBIC() {
        return BIC;
    }

    public String getName() {
        return name;
    }

    public void setBIC(String BIC) {
        this.BIC = BIC;
    }

    public void setName(String name) {
        this.name = name;
    }
}
