package com.progressoft.jip;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class Pacs008ParserTest {
    private Pacs008Parser parser;
    private Pacs008 pojo;

    @BeforeEach
    public void init() {
        parser = new Pacs008Parser();
        pojo = parser.parse(getXml());
    }

    @Test
    public void can_generate() {
        new Pacs008Parser();
    }

    @Test
    public void givenNullXML_whenObjectGenerator_thenThrowException() {
        NullPointerException exception = assertThrows(NullPointerException.class, () -> parser.parse(null));
        assertEquals("The XML cannot be null", exception.getMessage());
    }

    @Test
    public void givenPacs008xml_whenObjectGenerator_thenShouldGenerateObjectAndSetItsValues() {
        assertGroupHeader(pojo);
        assertCreditTransferTransactionInfo(pojo);
        assertPaymentIdentification(pojo);
        assertPaymentTypeInfo(pojo);
    }

    private void assertGroupHeader(Pacs008 pojo) {
        assertEquals("JGBA2812094750927334298", pojo.getMessageID(), "The messageId is not as expected");
        assertEquals("2020-12-28T07:47:50Z", pojo.getCreationDateTime(), "The creationDateTime is not as expected");
        assertEquals(1, pojo.getNumberOfTransactions());
    }

    private void assertPaymentTypeInfo(Pacs008 pojo) {
        assertEquals("RTNS", pojo.getClrChanl(), "The ClrChanl is not as expected");
        assertEquals("0100", pojo.getServiceLevel(), "The ServiceLevel-Prtry is not as expected");
        assertEquals("CSDC", pojo.getLocalInstrument(), "The LocalInstrument-Prtry is not as expected");
        assertEquals("11110", pojo.getCategoryPurpose(), "The CategoryPurpose-Prtry is not as expected");
    }

    private void assertPaymentIdentification(Pacs008 pojo) {
        assertEquals("JGBA2812094750927334298", pojo.getInstrId(), "The InstrId is not as expected");
        assertEquals("NOTPROVIDED", pojo.getEndToEndId(), "The EndToEndId is not as expected");
        assertEquals("JGBA2812094750927334297", pojo.getTxtId(), "The TxtId is not as expected");
    }

    private void assertCreditTransferTransactionInfo(Pacs008 pojo) {
        assertEquals("CLRG", pojo.getSettlementMethod(), "The SettlementMethod is not as expected");
        assertEquals("2021-10-09", pojo.getIntrBkSttlmDt(), "The InstrBkSttlmDt is not as expected");
        assertEquals("SLEV", pojo.getChargeBearer(), "The chargeBearer is not as expected");
        assertEquals("JGBAJOA0", pojo.getInstructingBICFI(), "The InstructingAgentD-BICFI is not as expected");
        assertEquals("HBHOJOA0", pojo.getInstructedBICFI(), "The InstructingAgentC-BICFI is not as expected");
        assertEquals("NISREEN MOHAMMAD YOUSEF", pojo.getDebtorName(), "The debtor name is not as expected");
        assertEquals("new zarqa jordan ", pojo.getDebtorAddress(), "The debtor address is not as expected");
        assertEquals("JO93JGBA6010000290450010010000", pojo.getDebtorIBAN(), "The debtor IBAN is not as expected");
        assertEquals("JGBAJOA0", pojo.getDebtorBICFI(), "The debtor-BICFI is not as expected");
        assertEquals("HBHOJOA0", pojo.getCreditorBICFI(), "The creditor-BICFI is not as expected");
        assertEquals("MAEN SAMI HATTAR SALEM", pojo.getCreditorName(), "The creditor name is not as expected");
        assertEquals("Amman Jordan SLT JO SLT ", pojo.getCreditorAddress(), "The creditor address is not as expected");
        assertEquals("JO83HBHO0320000033330600101001", pojo.getCreditorIBAN(), "The creditor IBAN is not as expected");
    }

    private String getXml() {
        return "<Document>" +
                "    <FIToFICstmrCdtTrf>\n" +
                "        <GrpHdr>\n" +
                "            <MsgId>JGBA2812094750927334298</MsgId>\n" +
                "            Unique message id\n" +
                "            <CreDtTm>2020-12-28T07:47:50Z</CreDtTm>\n" +
                "            <NbOfTxs>1</NbOfTxs>\n" +
                "            Fixed\n" +
                "            <SttlmInf>\n" +
                "                <SttlmMtd>CLRG</SttlmMtd>\n" +
                "            </SttlmInf>\n" +
                "        </GrpHdr>\n" +
                "        <CdtTrfTxInf>\n" +
                "            <PmtId>\n" +
                "                <InstrId>JGBA2812094750927334298</InstrId>\n" +
                "                <EndToEndId>NOTPROVIDED</EndToEndId>\n" +
                "                <TxId>JGBA2812094750927334297</TxId>\n" +
                "            </PmtId>\n" +
                "            <PmtTpInf>\n" +
                "                <ClrChanl>RTNS</ClrChanl>\n" +
                "                Fixed\n" +
                "                <SvcLvl>\n" +
                "                    <Prtry>0100</Prtry>\n" +
                "                    Fixed\n" +
                "                </SvcLvl>\n" +
                "                <LclInstrm>\n" +
                "                    <Prtry>CSDC</Prtry>\n" +
                "                    Fixed\n" +
                "                </LclInstrm>\n" +
                "                <CtgyPurp>\n" +
                "                    <Prtry>11110</Prtry>\n" +
                "                </CtgyPurp>\n" +
                "            </PmtTpInf>\n" +
                "            <IntrBkSttlmAmt Ccy=\"JOD\">1</IntrBkSttlmAmt>\n" +
                "            <IntrBkSttlmDt>2021-10-09</IntrBkSttlmDt>\n" +
                "            <ChrgBr>SLEV</ChrgBr>\n" +
                "            <InstgAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>JGBAJOA0</BICFI>\n" +
                "                    Sending participant (Debtor)\n" +
                "                </FinInstnId>\n" +
                "            </InstgAgt>\n" +
                "            <InstdAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                    Receiving participant (creditor)\n" +
                "                </FinInstnId>\n" +
                "            </InstdAgt>\n" +
                "            <Dbtr>\n" +
                "                Debtor information\n" +
                "                <Nm>NISREEN MOHAMMAD YOUSEF</Nm>\n" +
                "                <PstlAdr>\n" +
                "                    <AdrLine>new zarqa</AdrLine>\n" +
                "  <AdrLine>jordan</AdrLine>\\n\" " +
                "                </PstlAdr>\n" +
                "            </Dbtr>\n" +
                "            <DbtrAcct>\n" +
                "                <Id>\n" +
                "                <IBAN>JO93JGBA6010000290450010010000</IBAN>\n" +
                "                </Id>\n" +
                "                Debtor IBAN\n" +
                "            </DbtrAcct>\n" +
                "            <DbtrAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>JGBAJOA0</BICFI>\n" +
                "                    Debtor agent, should always be as InstgAgt\n" +
                "                </FinInstnId>\n" +
                "            </DbtrAgt>\n" +
                "            <CdtrAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                    Creditor agent, should always be as InstdAgnt\n" +
                "                </FinInstnId>\n" +
                "            </CdtrAgt>\n" +
                "            <Cdtr>\n" +
                "                Creditor information\n" +
                "                <Nm>MAEN SAMI HATTAR SALEM</Nm>\n" +
                "                <PstlAdr>\n" +
                "                    <AdrLine>Amman Jordan SLT JO SLT</AdrLine>\n" +
                "                    Could have a repetition with multiple address\n" +
                "                    lines\n" +
                "                </PstlAdr>\n" +
                "            </Cdtr>\n" +
                "            <CdtrAcct>\n" +
                "                <Id>\n" +
                "                    <IBAN>JO83HBHO0320000033330600101001</IBAN>\n" +
                "                </Id>\n" +
                "                Creditor IBAN\n" +
                "            </CdtrAcct>\n" +
                "        </CdtTrfTxInf>\n" +
                "    </FIToFICstmrCdtTrf>\n" +
                "</Document>";
    }
}
