package com.progressoft.jip;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.*;
import java.io.File;

import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Pacs008StructureValidator {
    private final Logger logger = Logger.getLogger(Pacs008StructureValidator.class.getName());

    public boolean validate(String xml) throws URISyntaxException {
        xmlValidator(xml);

        File fileToValidate = getFile("structureValidation/xmlToValidate.xml");
        File xsdFile = getFile("pacs8.xsd");
        Source schemaFile = new StreamSource(xsdFile);

        appendTheXmlToValidationFile(xml, fileToValidate);

        Source xmlFile = new StreamSource(fileToValidate);
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        try {
            Schema schema = schemaFactory.newSchema(schemaFile);
            Validator validator = schema.newValidator();
            validator.validate(xmlFile);
            System.out.println(xmlFile.getSystemId() + " is valid pacs008");
            return true;
        } catch (SAXException e) {
            logger.log(Level.WARNING, xmlFile.getSystemId() + " is NOT valid pacs008 reason:" + e);
            return false;
        } catch (IOException e) {
            return false;
        }
    }

    private File getFile(String path) throws URISyntaxException {
        URL res = Pacs008StructureValidator.class.getClassLoader().getResource(path);
        assert res != null;

        return Paths.get(res.toURI()).toFile();
    }

    private void xmlValidator(String xml) {
        if (xml == null)
            throw new NullPointerException("Null xml");
        if (xml.equals(""))
            throw new IllegalArgumentException("Empty xml");
    }

    private void appendTheXmlToValidationFile(String xml, File fileToValidate) {
        FileWriter writer = new FileWriter();
        writer.write(fileToValidate, xml);
    }

}
