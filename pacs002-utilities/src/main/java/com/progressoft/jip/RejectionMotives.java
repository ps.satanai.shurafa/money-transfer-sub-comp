package com.progressoft.jip;

public enum RejectionMotives {
    AC02("Debtor account number is invalid or missing"),
    AC03("Creditor account number invalid or missing"),
    FF03("Payment Type Information is missing or invalid.");

    public final String label;

    RejectionMotives(String label) {
        this.label = label;
    }
}
