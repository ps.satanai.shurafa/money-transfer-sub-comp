package com.progressoft.jip;

import com.progressoft.jip.entities.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Random;

import static org.mockito.Mockito.mock;

public class Pacs008HandlerTest {
    private Pacs008Handler handler;

    @BeforeEach
    void setUp() {
        Serializer serializer = new Pacs002Serializer();
        Pacs008Parser parser = new Pacs008Parser();

        MessagesRepository messagesRepository = mock(MessagesRepository.class);
        AccountsRepository accountsRepository = mock(AccountsRepository.class);
        ParticipantsRepository participantsRepository = mock(ParticipantsRepository.class);
        PurposeCodesRepository purposeCodesRepository = mock(PurposeCodesRepository.class);
        CurrenciesRepository currenciesRepository = mock(CurrenciesRepository.class);

        Pacs008ValuesValidator validator = new Pacs008ValuesValidator(messagesRepository, participantsRepository,
                accountsRepository, currenciesRepository, purposeCodesRepository);
        handler = new Pacs008Handler(serializer, parser, validator, messagesRepository);
    }

    @Test
    public void givenNullXml_whenStringHandling_thenThrowException() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> handler.handle((String) null));
        Assertions.assertEquals("Xml cannot be null", exception.getMessage());
    }

    @Test
    public void givenNullXml_whenFileHandling_thenThrowException() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> handler.handle((File) null));
        Assertions.assertEquals("Xml file cannot be null", exception.getMessage());

    }

    @Test
    public void givenNotExistFile_whenFileHandling_thenThrowException() {
        File file = new File(".", "dir" + new Random().nextInt());
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> handler.handle(file));
        Assertions.assertEquals("File does not exist", exception.getMessage());

    }

    @Test
    public void givenDirectory_whenFileHandling_thenThrowException() throws IOException {
        Path path = Files.createTempDirectory("mtp-dir");
        File file = new File(String.valueOf(path));
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> handler.handle(file));
        Assertions.assertEquals("File cannot be a directory", exception.getMessage());

    }

    @Test
    public void givenValidXml_whenStringHandling_thenShouldReturnResponseData() {
        ResponseData responseData = handler.handle(getXml());
        Assertions.assertNotNull(responseData.getXml());
        Assertions.assertEquals("JGBA0009404750927330560", responseData.getMsgId());
    }

    private String getXml() {
        return "<Document xmlns=\"urn:iso:std:iso:20022:tech:xsd:pacs.008.001.10\">\n" +
                "    <FIToFICstmrCdtTrf>\n" +
                "        <GrpHdr>\n" +
                "            <MsgId>JGBA0009404750927330560</MsgId>\n" +
                "            <CreDtTm>2020-12-28T07:47:50Z</CreDtTm>\n" +
                "            <NbOfTxs>1</NbOfTxs>\n" +
                "            <SttlmInf>\n" +
                "                <SttlmMtd>CLRG</SttlmMtd>\n" +
                "            </SttlmInf>\n" +
                "        </GrpHdr>\n" +
                "        <CdtTrfTxInf>\n" +
                "            <PmtId>\n" +
                "                <InstrId>JGBA2812094750927334298</InstrId>\n" +
                "                <EndToEndId>NOTPROVIDED</EndToEndId>\n" +
                "                <TxId>JGBA2812094750927334297</TxId>\n" +
                "            </PmtId>\n" +
                "            <PmtTpInf>\n" +
                "                <ClrChanl>RTNS</ClrChanl>\n" +
                "                <SvcLvl>\n" +
                "                    <Prtry>0100</Prtry>\n" +
                "                </SvcLvl>\n" +
                "                <LclInstrm>\n" +
                "                    <Prtry>CSDC</Prtry>\n" +
                "                </LclInstrm>\n" +
                "                <CtgyPurp>\n" +
                "                    <Prtry>11110</Prtry>\n" +
                "                </CtgyPurp>\n" +
                "            </PmtTpInf>\n" +
                "            <IntrBkSttlmAmt Ccy=\"JOD\">1</IntrBkSttlmAmt>\n" +
                "            <IntrBkSttlmDt>2022-07-06</IntrBkSttlmDt>\n" +
                "            <ChrgBr>SLEV</ChrgBr>\n" +
                "            <InstgAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>JGBAJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstgAgt>\n" +
                "            <InstdAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstdAgt>\n" +
                "            <Dbtr>\n" +
                "                <Nm>NISREEN MOHAMMAD YOUSEF</Nm>\n" +
                "                <PstlAdr>\n" +
                "                    <AdrLine>new zarqa</AdrLine>\n" +
                "                </PstlAdr>\n" +
                "            </Dbtr>\n" +
                "            <DbtrAcct>\n" +
                "                <Id>\n" +
                "                    <IBAN>JO93JGBA6010000290450010010000</IBAN>\n" +
                "                </Id>\n" +
                "            </DbtrAcct>\n" +
                "            <DbtrAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>JGBAJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </DbtrAgt>\n" +
                "            <CdtrAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </CdtrAgt>\n" +
                "            <Cdtr>\n" +
                "                <Nm>MAEN SAMI HATTAR SALEM</Nm>\n" +
                "                <PstlAdr>\n" +
                "                    <AdrLine>Amman Jordan SLT JO SLT</AdrLine>\n" +
                "                </PstlAdr>\n" +
                "            </Cdtr>\n" +
                "            <CdtrAcct>\n" +
                "                <Id>\n" +
                "                    <IBAN>JO83HBHO0320000033330600101001</IBAN>\n" +
                "                </Id>\n" +
                "            </CdtrAcct>\n" +
                "        </CdtTrfTxInf>\n" +
                "    </FIToFICstmrCdtTrf>\n" +
                "</Document>";
    }
}
