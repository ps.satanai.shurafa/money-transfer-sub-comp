package com.progressoft.jip;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType( propOrder = {"originalInstructionIdentification",
        "originalEndToEndIdentification",
        "originalTransactionIdentification",
        "stsRsnInf",
        "instgAgt",
        "instdAgt",
        "orgnlTxRef"
})
public class TransactionInformationAndStatus {
    private String originalInstructionIdentification;
    private String originalEndToEndIdentification;
    private String originalTransactionIdentification;
    private InstructingAgent instgAgt;
    private InstructedAgent instdAgt;
    private OriginalTransactionReference orgnlTxRef;

    private StatusReasonInformation StsRsnInf;

    public StatusReasonInformation getStsRsnInf() {
        return StsRsnInf;
    }


    @XmlElement(name = "StsRsnInf")
    public void setStsRsnInf(StatusReasonInformation stsRsnInf) {
        StsRsnInf = stsRsnInf;
    }

    @XmlElement(name = "OrgnlInstrId")
    public void setOriginalInstructionIdentification(String originalInstructionIdentification) {
        this.originalInstructionIdentification = originalInstructionIdentification;
    }

    @XmlElement(name = "OrgnlEndToEndId")
    public void setOriginalEndToEndIdentification(String originalEndToEndIdentification) {
        this.originalEndToEndIdentification = originalEndToEndIdentification;
    }

    @XmlElement(name = "OrgnlTxId")
    public void setOriginalTransactionIdentification(String originalTransactionIdentification) {
        this.originalTransactionIdentification = originalTransactionIdentification;
    }

    @XmlElement(name = "InstgAgt")
    public void setInstgAgt(InstructingAgent instgAgt) {
        this.instgAgt = instgAgt;
    }

    @XmlElement(name = "InstdAgt")
    public void setInstdAgt(InstructedAgent instdAgt) {
        this.instdAgt = instdAgt;
    }

    @XmlElement(name = "OrgnlTxRef")
    public void setOrgnlTxRef(OriginalTransactionReference orgnlTxRef) {
        this.orgnlTxRef = orgnlTxRef;
    }

    public String getOriginalInstructionIdentification() {
        return originalInstructionIdentification;
    }

    public String getOriginalEndToEndIdentification() {
        return originalEndToEndIdentification;
    }

    public String getOriginalTransactionIdentification() {
        return originalTransactionIdentification;
    }

    public InstructingAgent getInstgAgt() {
        return instgAgt;
    }

    public InstructedAgent getInstdAgt() {
        return instdAgt;
    }

    public OriginalTransactionReference getOrgnlTxRef() {
        return orgnlTxRef;
    }
}
