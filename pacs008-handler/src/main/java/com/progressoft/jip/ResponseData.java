package com.progressoft.jip;

public class ResponseData {
    private String xml;
    private String msgId;
    private String responseType;
    private String desc;

    public String getXml() {
        return xml;
    }

    public String getMsgId() {
        return msgId;
    }

    public String getResponseType() {
        return responseType;
    }

    public String getDesc() {
        return desc;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setXml(String xml) {
        this.xml = new XmlFormatter().format(xml);
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

}
