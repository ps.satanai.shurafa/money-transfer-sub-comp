package com.progressoft.jip;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.net.URI;


@XmlRootElement(name = "Document")
public class Pacs002Doc {
    private FIToFIPmtStsRpt fiToFIPmtStsRpt;
    private URI xmlns;


    public URI getXmlns() {
        return xmlns;
    }
    @XmlAttribute(name = "Xmlns")
    public void setXmlns(URI xmlns) {
        this.xmlns = xmlns;
    }

    @XmlElement(name = "FIToFIPmtStsRpt")
    public void setFiToFIPmtStsRpt(FIToFIPmtStsRpt fiToFIPmtStsRpt) {
        this.fiToFIPmtStsRpt = fiToFIPmtStsRpt;
    }

    public FIToFIPmtStsRpt getFiToFIPmtStsRpt() {
        return fiToFIPmtStsRpt;
    }
}
