package com.progressoft.jip;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType( propOrder = {"rsn",
        "additionalInformation"
})
public class StatusReasonInformation {
    private Reson rsn;
    private String AdditionalInformation;

    @XmlElement(name = "AddtlInf")
    public void setAdditionalInformation(String additionalInformation) {
        AdditionalInformation = additionalInformation;
    }

    public String getAdditionalInformation() {
        return AdditionalInformation;
    }

    @XmlElement(name = "Rsn")
    public void setRsn(Reson rsn) {
        this.rsn = rsn;
    }

    public Reson getRsn() {
        return rsn;
    }
}
