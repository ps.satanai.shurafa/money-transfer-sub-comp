package com.progressoft.jip;

import java.util.Random;

public class MessageIdGenerator {
    public String generate(String BIC) {
        Random rand = new Random();
        int firstPart = rand.nextInt(10);
        int secondPart1 = rand.nextInt(999999 - 100000) + 100000;
        int secondPart2 = rand.nextInt(999999 - 100000) + 100000;
        int thirdPart = rand.nextInt(9999 - 1000) + 1000;

        return BIC + "-" + firstPart + "-" + secondPart1 + "" + secondPart2 + "-" + thirdPart;
    }
}
