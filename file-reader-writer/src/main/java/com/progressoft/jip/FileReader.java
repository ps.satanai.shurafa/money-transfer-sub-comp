package com.progressoft.jip;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileReader {
    public String read(File xmlFile) {
        StringBuilder xml = new StringBuilder();
        validateFile(xmlFile);

        try (Scanner myReader = new Scanner(xmlFile)) {
            while (myReader.hasNextLine()) {
                xml.append("\n").append(myReader.nextLine());
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            e.printStackTrace();
        }
        return xml.toString();
    }

    private void validateFile(File xmlFile) {
        if (xmlFile == null)
            throw new NullPointerException("File cannot be null");
        if (!xmlFile.exists())
            throw new IllegalArgumentException("File does not exist");
        if (xmlFile.isDirectory())
            throw new IllegalArgumentException("Cannot be a directory");
    }


}

