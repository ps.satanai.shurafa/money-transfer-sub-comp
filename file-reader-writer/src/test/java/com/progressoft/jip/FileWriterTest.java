package com.progressoft.jip;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Random;
import java.util.Scanner;

public class FileWriterTest {
    @Test
    public void can_create() {
        new FileWriter();
    }

    @Test
    public void givenNullFile_whenWrite_thenFail() {
        FileWriter writer = new FileWriter();
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> writer.write(null, "Hi"));
        Assertions.assertEquals("File cannot be null", exception.getMessage());
    }

    @Test
    public void givenNullXml_whenWrite_thenFail() {
        FileWriter writer = new FileWriter();
        File file = new File(".", "dir" + new Random().nextInt());
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> writer.write(file, null));
        Assertions.assertEquals("Xml cannot be null", exception.getMessage());
    }


    @Test
    public void givenValidFileToWriteAndNotNullXml_whenWrite_ThenSuccess() throws URISyntaxException {
        FileWriter writer = new FileWriter();
        URL res = FileWriterTest.class.getClassLoader().getResource("writer-test.txt");
        File file = null;
        if (res != null) {
            file = Paths.get(res.toURI()).toFile();
        }
        writer.write(file, "Hello world");
        String expected = "Hello world";
        String result = getResult(file);

        Assertions.assertEquals(expected, result);
    }

    private String getResult(File file) {
        StringBuilder result = new StringBuilder();
        try (Scanner myReader = new Scanner(file)) {
            while (myReader.hasNextLine()) {
                result.append(myReader.nextLine());
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            e.printStackTrace();
        }
        return result.toString();
    }
}
