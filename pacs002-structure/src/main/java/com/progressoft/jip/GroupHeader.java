package com.progressoft.jip;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType( propOrder = {"messageId",
        "originalCreationDateTime",
        "instructingAgent",
        "instructedAgent"
})
public class GroupHeader {
    private String messageId;
    private String originalCreationDateTime;
    private InstructingAgent instructingAgent;
    private InstructedAgent instructedAgent;

    @XmlElement(name = "MsgId")
    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    @XmlElement(name = "CreDtTm")
    public void setOriginalCreationDateTime(String originalCreationDateTime) {
        this.originalCreationDateTime = originalCreationDateTime;
    }

    @XmlElement(name = "InstgAgt")
    public void setInstructingAgent(InstructingAgent instructingAgent) {
        this.instructingAgent = instructingAgent;
    }

    @XmlElement(name = "InstdAgt")
    public void setInstructedAgent(InstructedAgent instructedAgent) {
        this.instructedAgent = instructedAgent;
    }

    public String getMessageId() {
        return messageId;
    }

    public String getOriginalCreationDateTime() {
        return originalCreationDateTime;
    }

    public InstructingAgent getInstructingAgent() {
        return instructingAgent;
    }

    public InstructedAgent getInstructedAgent() {
        return instructedAgent;
    }

}
