package com.progressoft.jip;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ResponseGeneratorTest {
    private ResponseGenerator generator;

    @BeforeEach
    void setUp() {
        generator = new ResponseGenerator();
    }

    @Test
    public void can_create() {
        new ResponseGeneratorTest();
    }

    @Test
    public void givenNullPacs008_whenGenerate_thenThrowException() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> generator.generate(null, RejectionMotives.FF03, "invalid info"));
        Assertions.assertEquals("Pacs008 cannot be null", exception.getMessage());
    }

    @Test
    public void givenNullReasonAndNotNullDesc_whenGenerate_thenThrowException() {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> generator.generate(getPacs008(), null, "invalid info"));
        Assertions.assertEquals("shouldn't be a reason desc because its AUTH pacs002", exception.getMessage());
    }

    @Test
    public void givenNullReasonDescAndNotNullReason_whenGenerate_thenThrowException() {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> generator.generate(getPacs008(), RejectionMotives.FF03, null));
        Assertions.assertEquals("there should be a description for the rejection", exception.getMessage());
    }

    @Test
    public void givenNullReasonAndDesc_whenGenerate_thenTheResponseShouldBeAUTH() {
        generator.generate(getPacs008(), null, null);
        Assertions.assertEquals(Pacs002Types.AUTH, generator.getResponseType());
    }

    @Test
    public void givenNotNullReasonAndDesc_whenGenerate_thenTheResponseShouldBeNAUT() {
        generator.generate(getPacs008(), RejectionMotives.FF03, "InvalidInfo");
        Assertions.assertEquals(Pacs002Types.NAUT, generator.getResponseType());
    }

    @Test
    public void givenNullReasonAndDesc_whenGenerate_thenShouldGenerateAUTH() {
        Pacs008 pojo = getPacs008();
        Pacs002 pacs002 = generator.generate(pojo, null, null);

        Assertions.assertEquals("AUTH", pacs002.getResponseType());
        assertAUTH(pojo, pacs002);
    }

    @Test
    public void givenNotNullReasonAndDesc_whenGenerate_thenShouldGenerateNAUT() {
        Pacs008 pojo = getPacs008();
        Pacs002 pacs002 = generator.generate(pojo, RejectionMotives.FF03, "Invalid Info");

        assertAUTH(pojo, pacs002);
        Assertions.assertEquals("NAUT", pacs002.getResponseType());
        Assertions.assertEquals("FF03", pacs002.getRejectionReson());
        Assertions.assertEquals("Invalid Info", pacs002.getRejectionAdditionalInfo());
    }

    private void assertAUTH(Pacs008 pojo, Pacs002 pacs002) {
        Assertions.assertEquals(pojo.getCreditorBICFI(), pacs002.getInstructdAgentBICFI());
        Assertions.assertEquals("ZYAAJOA0AIPS", pacs002.getInstructedMemberIdentification());
        Assertions.assertEquals(pojo.getMessageID(), pacs002.getOriginalMessageIdentification());
        Assertions.assertEquals("pacs.008.001.08", pacs002.getOriginalMessageNameIdentification());
        Assertions.assertEquals(pojo.getInstrId(), pacs002.getOriginalInstructionIdentification());
        Assertions.assertEquals(pojo.getEndToEndId(), pacs002.getOriginalEndToEndIdentification());
        Assertions.assertEquals(pojo.getTxtId(), pacs002.getOriginalTransactionIdentification());
        Assertions.assertEquals(pojo.getDebtorBICFI(), pacs002.getInstructgAgentBICFI());
        Assertions.assertEquals(pojo.getCreditorBICFI(), pacs002.getInstructdAgentBICFI());
        Assertions.assertEquals(pojo.getCurrency(), pacs002.getCurrency());
        Assertions.assertEquals(pojo.getAmount(), pacs002.getAmount());
        Assertions.assertEquals(pojo.getIntrBkSttlmDt(), pacs002.getInterbankSettlementDate());
    }

    private Pacs008 getPacs008() {
        Pacs008 pojo = new Pacs008();
        pojo.setMessageID("JGBA1234594780927330523");
        pojo.setCreationDateTime("2020-12-28T07:47:50Z");
        pojo.setNumberOfTransactions(1);
        pojo.setSettlementMethod("CLRG");
        pojo.setInstrId("JGBA2812094750927334298");
        pojo.setEndToEndId("NOTPROVIDED");
        pojo.setTxtId("JGBA2812094750927334297");
        pojo.setClrChanl("RTNS");
        pojo.setServiceLevel("0100");
        pojo.setLocalInstrument("CSDC");
        pojo.setCategoryPurpose("11110");
        pojo.setCurrency("JOD");
        pojo.setAmount("1");
        pojo.setChargeBearer("SLEV");
        pojo.setInstructingBICFI("JGBAJOA0");
        pojo.setInstructedBICFI("HBHOJOA0");
        pojo.setDebtorName("NISREEN MOHAMMAD YOUSEF");
        pojo.setDebtorAddress("new zarqa");
        pojo.setDebtorIBAN("JO93JGBA6010000290450010010000");
        pojo.setDebtorBICFI("JGBAJOA0");
        pojo.setCreditorBICFI("HBHOJOA0");
        pojo.setCreditorName("MAEN SAMI HATTAR SALEM");
        pojo.setCreditorAddress("Amman Jordan SLT JO SLT");
        pojo.setCreditorIBAN("JO83HBHO0320000033330600101001");
        pojo.setIntrBkSttlmDt("2022-07-06");
        return pojo;
    }

}
