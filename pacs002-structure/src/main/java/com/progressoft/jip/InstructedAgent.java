package com.progressoft.jip;

import javax.xml.bind.annotation.XmlElement;

public class InstructedAgent {
    private FinancialInstitutionIdentification finInstnId;

    @XmlElement(name = "FinInstnId")
    public void setFinInstnId(FinancialInstitutionIdentification finInstnId) {
        this.finInstnId = finInstnId;
    }

    public FinancialInstitutionIdentification getFinInstnId() {
        return finInstnId;
    }
}

