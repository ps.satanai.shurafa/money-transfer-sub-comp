package com.progressoft.jip;

import org.h2.jdbcx.JdbcDataSource;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class DatabaseInitializerTest {
    @Test
    public void givenValidDatasource_whenInitialize_thenSuccess() throws IOException, SQLException {
        DatabaseInitializer initializer = new DatabaseInitializer();
        JdbcDataSource dataSource = new JdbcDataSource();

        Path mtp_db = Files.createTempFile("mtp_db", ".db");
        System.out.println(mtp_db);
        dataSource.setURL("jdbc:h2:file:" + mtp_db);
        dataSource.setUser("root");
        dataSource.setPassword("root");

        initializer.initialize(dataSource);

        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement("select * from participants")) {
                boolean execute = statement.execute();
                assertTrue(execute);
            }
        }

    }
}
