package com.progressoft;

import javax.xml.bind.annotation.XmlElement;

public class FIToFICustomerCreditTransfer {
    private GroupHeader groupHeader;
    private CreditTransferTransactionInformation CdtTrfTxInf;

    @XmlElement(name = "GrpHdr")
    public void setGroupHeader(GroupHeader groupHeader) {
        this.groupHeader = groupHeader;
    }

    @XmlElement(name = "CdtTrfTxInf")
    public void setCdtTrfTxInf(CreditTransferTransactionInformation cdtTrfTxInf) {
        CdtTrfTxInf = cdtTrfTxInf;
    }

    public GroupHeader getGroupHeader() {
        return groupHeader;
    }

    public CreditTransferTransactionInformation getCdtTrfTxInf() {
        return CdtTrfTxInf;
    }
}
