package com.progressoft.jip;

public interface Serializer {
    String serialize(Pacs002 pojo);
}
