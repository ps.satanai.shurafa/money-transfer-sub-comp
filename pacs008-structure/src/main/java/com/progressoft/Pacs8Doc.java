package com.progressoft;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.net.URI;

@XmlRootElement(name = "Document")
public class Pacs8Doc {
    private URI xmlns;
    private FIToFICustomerCreditTransfer fIToFICstmrCdtTrf;

    @XmlAttribute(name = "Xmlns")
    public void setXmlns(String xmlns) {
        this.xmlns = URI.create(xmlns);
    }

    @XmlElement(name = "FIToFICstmrCdtTrf")
    public void setfIToFICstmrCdtTrf(FIToFICustomerCreditTransfer fIToFICstmrCdtTrf) {
        this.fIToFICstmrCdtTrf = fIToFICstmrCdtTrf;
    }

    public URI getXmlns() {
        return xmlns;
    }

    public FIToFICustomerCreditTransfer getfIToFICstmrCdtTrf() {
        return fIToFICstmrCdtTrf;
    }
}
