package com.progressoft.jip;

public interface Parser {
    Pacs008 parse(String xml);
}
