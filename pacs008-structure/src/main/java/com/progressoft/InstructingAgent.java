package com.progressoft;

import javax.xml.bind.annotation.XmlElement;

public class InstructingAgent {
    private FinancialInstitutionIdentification finInstnId;
    public void setFinInstnId(FinancialInstitutionIdentification finInstnId) {
        this.finInstnId = finInstnId;
    }

    @XmlElement(name = "FinInstnId")
    public FinancialInstitutionIdentification getFinInstnId() {
        return finInstnId;
    }
}
