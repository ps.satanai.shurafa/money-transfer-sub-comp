package com.progressoft.jip;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class Pacs002SerializerTest {
    private Pacs002Serializer serialize;

    @BeforeEach
    void setUp() {
        serialize = new Pacs002Serializer();
    }

    @Test
    public void givenUnAuthPacs002Pojo_whenSerialize_thenGenerateXmlPacs002() {
        Pacs002 pacs002 = getUnAuthPacs002Pojo();
        String pacs002Xml = serialize.serialize(pacs002);
        String expectedXml = getExpectedUnauthXml();

        Assertions.assertEquals(expectedXml, pacs002Xml);
    }

    @Test
    public void givenAuthPacs002Pojo_whenSerialize_thenGenerateXmlPacs002() {
        Pacs002 pacs002 = getAuthPacs002Pojo();
        String pacs002Xml = serialize.serialize(pacs002);
        String expectedXml = getExpectedAuthXml();

        Assertions.assertEquals(expectedXml, pacs002Xml);
    }

    private String getExpectedAuthXml() {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
                "<Document>" +
                "<FIToFIPmtStsRpt>" +
                "<GrpHdr>" +
                "<MsgId>HBHOJOA0-1-210414151954-001</MsgId>" +
                "<CreDtTm>2021-10-09T15:19:54Z</CreDtTm>" +
                "<InstgAgt>" +
                "<FinInstnId>" +
                "<BICFI>HBHOJOA0</BICFI>" +
                "</FinInstnId>" +
                "</InstgAgt>" +
                "<InstdAgt>" +
                "<FinInstnId>" +
                "<ClrSysMmbId>" +
                "<MmbId>ZYAAJOA0AIPS</MmbId>" +
                "</ClrSysMmbId>" +
                "</FinInstnId>" +
                "</InstdAgt>" +
                "</GrpHdr>" +
                "<OrgnlGrpInfAndSts>" +
                "<OrgnlMsgId>JGBA2812094750927334298</OrgnlMsgId>" +
                "<OrgnlMsgNmId>pacs.008.001.08</OrgnlMsgNmId>" +
                "<StsRsnInf>" +
                "<Rsn>" +
                "<Prtry>AUTH</Prtry>" +
                "</Rsn>" +
                "</StsRsnInf>" +
                "</OrgnlGrpInfAndSts>" +
                "<TxInfAndSts>" +
                "<OrgnlInstrId>JGBA2812094750927334298</OrgnlInstrId>" +
                "<OrgnlEndToEndId>NOTPROVIDED</OrgnlEndToEndId>" +
                "<OrgnlTxId>JGBA2812094750927334298</OrgnlTxId>" +
                "<InstgAgt>" +
                "<FinInstnId>" +
                "<BICFI>JGBAJOA0</BICFI>" +
                "</FinInstnId>" +
                "</InstgAgt>" +
                "<InstdAgt>" +
                "<FinInstnId>" +
                "<BICFI>HBHOJOA0</BICFI>" +
                "</FinInstnId>" +
                "</InstdAgt>" +
                "<OrgnlTxRef>" +
                "<IntrBkSttlmAmt Ccy=\"JOD\">1.000</IntrBkSttlmAmt>" +
                "<IntrBkSttlmDt>2021-10-09</IntrBkSttlmDt>" +
                "</OrgnlTxRef>" +
                "</TxInfAndSts>" +
                "</FIToFIPmtStsRpt>" +
                "</Document>";
    }

    private Pacs002 getUnAuthPacs002Pojo() {
        Pacs002 pacs002 = new Pacs002();
        pacs002.setMessageId("HBHOJOA0-1-211009112732-6443");
        pacs002.setOriginalCreationDateTime("2021-10-09T11:27:32Z");
        pacs002.setInstructingAgentBICFI("HBHOJOA0");
        pacs002.setInstructedMemberIdentification("ZYAAJOA0AIPS");
        pacs002.setOriginalMessageIdentification("JGBA2812094750927334298");
        pacs002.setOriginalMessageNameIdentification("pacs.008.001.08");
        pacs002.setResponseType("NAUT");
        pacs002.setOriginalInstructionIdentification("JGBA2812094750927334298");
        pacs002.setOriginalEndToEndIdentification("NOTPROVIDED");
        pacs002.setOriginalTransactionIdentification("JGBA2812094750927334298");
        pacs002.setCurrency("JOD");
        pacs002.setAmount("1.000");
        pacs002.setInterbankSettlementDate("2021-10-09");
        pacs002.setRejectionReson("FF03");
        pacs002.setInstructgAgentBICFI("JGBAJOA0");
        pacs002.setInstructdAgentBICFI("HBHOJOA0");
        pacs002.setRejectionAdditionalInfo("Payment Type Information is missing or invalid");
        return pacs002;
    }

    private Pacs002 getAuthPacs002Pojo() {
        Pacs002 pacs002 = new Pacs002();
        pacs002.setMessageId("HBHOJOA0-1-210414151954-001");
        pacs002.setOriginalCreationDateTime("2021-10-09T15:19:54Z");
        pacs002.setInstructingAgentBICFI("HBHOJOA0");
        pacs002.setInstructedMemberIdentification("ZYAAJOA0AIPS");
        pacs002.setOriginalMessageIdentification("JGBA2812094750927334298");
        pacs002.setOriginalMessageNameIdentification("pacs.008.001.08");
        pacs002.setResponseType("AUTH");
        pacs002.setOriginalInstructionIdentification("JGBA2812094750927334298");
        pacs002.setOriginalEndToEndIdentification("NOTPROVIDED");
        pacs002.setOriginalTransactionIdentification("JGBA2812094750927334298");
        pacs002.setCurrency("JOD");
        pacs002.setAmount("1.000");
        pacs002.setInstructdAgentBICFI("HBHOJOA0");
        pacs002.setInstructgAgentBICFI("JGBAJOA0");
        pacs002.setInterbankSettlementDate("2021-10-09");
        return pacs002;
    }

    private String getExpectedUnauthXml() {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
                "<Document>" +
                "<FIToFIPmtStsRpt>" +
                "<GrpHdr>" +
                "<MsgId>HBHOJOA0-1-211009112732-6443</MsgId>" +
                "<CreDtTm>2021-10-09T11:27:32Z</CreDtTm>" +
                "<InstgAgt>" +
                "<FinInstnId>" +
                "<BICFI>HBHOJOA0</BICFI>" +
                "</FinInstnId>" +
                "</InstgAgt>" +
                "<InstdAgt>" +
                "<FinInstnId>" +
                "<ClrSysMmbId>" +
                "<MmbId>ZYAAJOA0AIPS</MmbId>" +
                "</ClrSysMmbId>" +
                "</FinInstnId>" +
                "</InstdAgt>" +
                "</GrpHdr>" +
                "<OrgnlGrpInfAndSts>" +
                "<OrgnlMsgId>JGBA2812094750927334298</OrgnlMsgId>" +
                "<OrgnlMsgNmId>pacs.008.001.08</OrgnlMsgNmId>" +
                "<StsRsnInf>" +
                "<Rsn>" +
                "<Prtry>NAUT</Prtry>" +
                "</Rsn>" +
                "</StsRsnInf>" +
                "</OrgnlGrpInfAndSts>" +
                "<TxInfAndSts>" +
                "<OrgnlInstrId>JGBA2812094750927334298</OrgnlInstrId>" +
                "<OrgnlEndToEndId>NOTPROVIDED</OrgnlEndToEndId>" +
                "<OrgnlTxId>JGBA2812094750927334298</OrgnlTxId>" +
                "<StsRsnInf>" +
                "<Rsn>" +
                "<Prtry>FF03</Prtry>" +
                "</Rsn>" +
                "<AddtlInf>Payment Type Information is missing or invalid</AddtlInf>" +
                "</StsRsnInf>" +
                "<InstgAgt>" +
                "<FinInstnId>" +
                "<BICFI>JGBAJOA0</BICFI>" +
                "</FinInstnId>" +
                "</InstgAgt>" +
                "<InstdAgt>" +
                "<FinInstnId>" +
                "<BICFI>HBHOJOA0</BICFI>" +
                "</FinInstnId>" +
                "</InstdAgt>" +
                "<OrgnlTxRef>" +
                "<IntrBkSttlmAmt Ccy=\"JOD\">1.000</IntrBkSttlmAmt>" +
                "<IntrBkSttlmDt>2021-10-09</IntrBkSttlmDt>" +
                "</OrgnlTxRef>" +
                "</TxInfAndSts>" +
                "</FIToFIPmtStsRpt>" +
                "</Document>";
    }
}
