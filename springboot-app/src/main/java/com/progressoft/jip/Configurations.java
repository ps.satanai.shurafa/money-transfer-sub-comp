package com.progressoft.jip;

import com.progressoft.jip.entities.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Configurations {
    @Bean
    public Pacs008Handler handler(Serializer serializer, Parser parser, ValuesValidator valuesValidator, MessagesRepository messagesRepository) {
        return new Pacs008Handler(serializer, parser, valuesValidator, messagesRepository);
    }

    @Bean
    public Parser parser() {
        return new Pacs008Parser();
    }

    @Bean
    public Serializer serializer() {
        return new Pacs002Serializer();
    }

    @Bean
    public ValuesValidator valuesValidator(MessagesRepository messagesRepository, ParticipantsRepository participantsRepository,
                                           AccountsRepository accountsRepository, CurrenciesRepository currenciesRepository, PurposeCodesRepository purposeCodesRepository) {
        return new Pacs008ValuesValidator(messagesRepository, participantsRepository, accountsRepository, currenciesRepository, purposeCodesRepository);
    }

}
