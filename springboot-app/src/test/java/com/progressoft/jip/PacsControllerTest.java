package com.progressoft.jip;

import org.junit.Assert;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PacsControllerTest extends SpringBootTests {
    private Pacs008Handler handler;
    private ResponseData responseData;
    private String pacs;
    private String expectedResponse;
    @Autowired
    private WebApplicationContext context;
    MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        handler = mock(Pacs008Handler.class);
        responseData = new ResponseData();
    }

    @Test
    public void givenEmptyMessage_whenSendingItToTheEndPoint_thenTheStatusShouldBe200AndReturnsErrorMessage() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();

        MvcResult result = mockMvc.perform(post("/response").param("pacs", " ").contentType(MediaType.APPLICATION_XML)).andExpect(status().isOk()).andReturn();
        String resultContent = result.getResponse().getContentAsString();
        Assert.assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<Error>THE MESSAGE CANNOT BE EMPTY!!!</Error>", resultContent);
    }

    @Test
    public void givenNullMessage_thenShouldReturnErrorMessage() {
        expectedResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<Error>THE MESSAGE CANNOT BE EMPTY!!!</Error>";

        PacsController controller = new PacsController(handler);
        String actualResponse = controller.getResponse(null);

        Assert.assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void givenValidPacsWithDuplicateMessageID_thenShouldReturnNautResponse() {
        pacs = getDuplicateIdPacs();
        expectedResponse = getHandleMocResultForDuplicateMsgID();

        responseData.setXml(expectedResponse);
        when(handler.handle(pacs)).thenReturn(responseData);

        PacsController controller = new PacsController(handler);
        String actualResponse = controller.getResponse(pacs);

        Assert.assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void givenValidPacs_thenShouldReturnAuthResponse() {
        pacs = getValidPacs();
        expectedResponse = getHandleMocResultForAuthResponse();

        responseData.setXml(expectedResponse);
        when(handler.handle(pacs)).thenReturn(responseData);

        PacsController controller = new PacsController(handler);
        String actualResponse = controller.getResponse(pacs);

        Assert.assertEquals(expectedResponse, actualResponse);
    }


    private String getValidPacs() {

        return "<Document xmlns=\"urn:iso:std:iso:20022:tech:xsd:pacs.008.001.10\">\n" +
                "    <FIToFICstmrCdtTrf>\n" +
                "        <GrpHdr>\n" +
                "            <MsgId>JGBA9991023754121115560</MsgId>\n" +
                "            <CreDtTm>2020-12-28T07:47:50Z</CreDtTm>\n" +
                "            <NbOfTxs>1</NbOfTxs>\n" +
                "            <SttlmInf>\n" +
                "                <SttlmMtd>CLRG</SttlmMtd>\n" +
                "            </SttlmInf>\n" +
                "        </GrpHdr>\n" +
                "        <CdtTrfTxInf>\n" +
                "            <PmtId>\n" +
                "                <InstrId>JGBA2810090750927334298</InstrId>\n" +
                "                <EndToEndId>NOTPROVIDED</EndToEndId>\n" +
                "                <TxId>JGBA2812094750927334297</TxId>\n" +
                "            </PmtId>\n" +
                "            <PmtTpInf>\n" +
                "                <ClrChanl>RTNS</ClrChanl>\n" +
                "                <SvcLvl>\n" +
                "                    <Prtry>0100</Prtry>\n" +
                "                </SvcLvl>\n" +
                "                <LclInstrm>\n" +
                "                    <Prtry>CSDC</Prtry>\n" +
                "                </LclInstrm>\n" +
                "                <CtgyPurp>\n" +
                "                    <Prtry>11110</Prtry>\n" +
                "                </CtgyPurp>\n" +
                "            </PmtTpInf>\n" +
                "            <IntrBkSttlmAmt Ccy=\"JOD\">1</IntrBkSttlmAmt>\n" +
                "            <IntrBkSttlmDt>2022-08-03</IntrBkSttlmDt>\n" +
                "            <ChrgBr>SLEV</ChrgBr>\n" +
                "            <InstgAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>JGBAJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstgAgt>\n" +
                "            <InstdAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstdAgt>\n" +
                "            <Dbtr>\n" +
                "                <Nm>NISREEN MOHAMMAD YOUSEF</Nm>\n" +
                "                <PstlAdr>\n" +
                "                    <AdrLine>new zarqa</AdrLine>\n" +
                "                </PstlAdr>\n" +
                "            </Dbtr>\n" +
                "            <DbtrAcct>\n" +
                "                <Id>\n" +
                "                    <IBAN>JO93JGBA6010000290450010010000</IBAN>\n" +
                "                </Id>\n" +
                "            </DbtrAcct>\n" +
                "            <DbtrAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>JGBAJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </DbtrAgt>\n" +
                "            <CdtrAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </CdtrAgt>\n" +
                "            <Cdtr>\n" +
                "                <Nm>MAEN SAMI HATTAR SALEM</Nm>\n" +
                "                <PstlAdr>\n" +
                "                    <AdrLine>Amman Jordan SLT JO SLT</AdrLine>\n" +
                "                </PstlAdr>\n" +
                "            </Cdtr>\n" +
                "            <CdtrAcct>\n" +
                "                <Id>\n" +
                "                    <IBAN>JO83HBHO0320000033330600101001</IBAN>\n" +
                "                </Id>\n" +
                "            </CdtrAcct>\n" +
                "        </CdtTrfTxInf>\n" +
                "    </FIToFICstmrCdtTrf>\n" +
                "</Document>";
    }

    private String getHandleMocResultForAuthResponse() {
        return "<Document>\n" +
                "    <FIToFIPmtStsRpt>\n" +
                "        <GrpHdr>\n" +
                "            <MsgId>HBHOJOA0-4-192433383721-7771</MsgId>\n" +
                "            <CreDtTm>2022-08-03T23:08:31Z</CreDtTm>\n" +
                "            <InstgAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstgAgt>\n" +
                "            <InstdAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <ClrSysMmbId>\n" +
                "                        <MmbId>ZYAAJOA0AIPS</MmbId>\n" +
                "                    </ClrSysMmbId>\n" +
                "                </FinInstnId>\n" +
                "            </InstdAgt>\n" +
                "        </GrpHdr>\n" +
                "        <OrgnlGrpInfAndSts>\n" +
                "            <OrgnlMsgId>JGBA9991023794121115560</OrgnlMsgId>\n" +
                "            <OrgnlMsgNmId>pacs.008.001.08</OrgnlMsgNmId>\n" +
                "            <StsRsnInf>\n" +
                "                <Rsn>\n" +
                "                    <Prtry>AUTH</Prtry>\n" +
                "                </Rsn>\n" +
                "            </StsRsnInf>\n" +
                "        </OrgnlGrpInfAndSts>\n" +
                "        <TxInfAndSts>\n" +
                "            <OrgnlInstrId>JGBA2810090750927334298</OrgnlInstrId>\n" +
                "            <OrgnlEndToEndId>NOTPROVIDED</OrgnlEndToEndId>\n" +
                "            <OrgnlTxId>JGBA2812094750927334297</OrgnlTxId>\n" +
                "            <InstgAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>JGBAJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstgAgt>\n" +
                "            <InstdAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstdAgt>\n" +
                "            <OrgnlTxRef>\n" +
                "                <IntrBkSttlmAmt Ccy=\"JOD\">1</IntrBkSttlmAmt>\n" +
                "                <IntrBkSttlmDt>2022-08-03</IntrBkSttlmDt>\n" +
                "            </OrgnlTxRef>\n" +
                "        </TxInfAndSts>\n" +
                "    </FIToFIPmtStsRpt>\n" +
                "</Document>\n";
    }

    private String getHandleMocResultForDuplicateMsgID() {
        return "<Document>\n" +
                "    <FIToFIPmtStsRpt>\n" +
                "        <GrpHdr>\n" +
                "            <MsgId>HBHOJOA0-1-787596708529-9144</MsgId>\n" +
                "            <CreDtTm>2022-08-03T20:58:28Z</CreDtTm>\n" +
                "            <InstgAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstgAgt>\n" +
                "            <InstdAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <ClrSysMmbId>\n" +
                "                        <MmbId>ZYAAJOA0AIPS</MmbId>\n" +
                "                    </ClrSysMmbId>\n" +
                "                </FinInstnId>\n" +
                "            </InstdAgt>\n" +
                "        </GrpHdr>\n" +
                "        <OrgnlGrpInfAndSts>\n" +
                "            <OrgnlMsgId>JGBA9991023754121115560</OrgnlMsgId>\n" +
                "            <OrgnlMsgNmId>pacs.008.001.08</OrgnlMsgNmId>\n" +
                "            <StsRsnInf>\n" +
                "                <Rsn>\n" +
                "                    <Prtry>NAUT</Prtry>\n" +
                "                </Rsn>\n" +
                "            </StsRsnInf>\n" +
                "        </OrgnlGrpInfAndSts>\n" +
                "        <TxInfAndSts>\n" +
                "            <OrgnlInstrId>JGBA2810090750927334298</OrgnlInstrId>\n" +
                "            <OrgnlEndToEndId>NOTPROVIDED</OrgnlEndToEndId>\n" +
                "            <OrgnlTxId>JGBA2812094750927334297</OrgnlTxId>\n" +
                "            <StsRsnInf>\n" +
                "                <Rsn>\n" +
                "                    <Prtry>FF03</Prtry>\n" +
                "                </Rsn>\n" +
                "                <AddtlInf>Duplicate message ID</AddtlInf>\n" +
                "            </StsRsnInf>\n" +
                "            <InstgAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>JGBAJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstgAgt>\n" +
                "            <InstdAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstdAgt>\n" +
                "            <OrgnlTxRef>\n" +
                "                <IntrBkSttlmAmt Ccy=\"JOD\">1</IntrBkSttlmAmt>\n" +
                "                <IntrBkSttlmDt>2022-07-31</IntrBkSttlmDt>\n" +
                "            </OrgnlTxRef>\n" +
                "        </TxInfAndSts>\n" +
                "    </FIToFIPmtStsRpt>\n" +
                "</Document>\n";
    }

    private String getDuplicateIdPacs() {
        return "<Document xmlns=\"urn:iso:std:iso:20022:tech:xsd:pacs.008.001.10\">\n" +
                "    <FIToFICstmrCdtTrf>\n" +
                "        <GrpHdr>\n" +
                "            <MsgId>JGBA9991023754121115560</MsgId>\n" +
                "            <CreDtTm>2020-12-28T07:47:50Z</CreDtTm>\n" +
                "            <NbOfTxs>1</NbOfTxs>\n" +
                "            <SttlmInf>\n" +
                "                <SttlmMtd>CLRG</SttlmMtd>\n" +
                "            </SttlmInf>\n" +
                "        </GrpHdr>\n" +
                "        <CdtTrfTxInf>\n" +
                "            <PmtId>\n" +
                "                <InstrId>JGBA2810090750927334298</InstrId>\n" +
                "                <EndToEndId>NOTPROVIDED</EndToEndId>\n" +
                "                <TxId>JGBA2812094750927334297</TxId>\n" +
                "            </PmtId>\n" +
                "            <PmtTpInf>\n" +
                "                <ClrChanl>RTNS</ClrChanl>\n" +
                "                <SvcLvl>\n" +
                "                    <Prtry>0100</Prtry>\n" +
                "                </SvcLvl>\n" +
                "                <LclInstrm>\n" +
                "                    <Prtry>CSDC</Prtry>\n" +
                "                </LclInstrm>\n" +
                "                <CtgyPurp>\n" +
                "                    <Prtry>11110</Prtry>\n" +
                "                </CtgyPurp>\n" +
                "            </PmtTpInf>\n" +
                "            <IntrBkSttlmAmt Ccy=\"JOD\">1</IntrBkSttlmAmt>\n" +
                "            <IntrBkSttlmDt>2022-07-31</IntrBkSttlmDt>\n" +
                "            <ChrgBr>SLEV</ChrgBr>\n" +
                "            <InstgAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>JGBAJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstgAgt>\n" +
                "            <InstdAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstdAgt>\n" +
                "            <Dbtr>\n" +
                "                <Nm>NISREEN MOHAMMAD YOUSEF</Nm>\n" +
                "                <PstlAdr>\n" +
                "                    <AdrLine>new zarqa</AdrLine>\n" +
                "                </PstlAdr>\n" +
                "            </Dbtr>\n" +
                "            <DbtrAcct>\n" +
                "                <Id>\n" +
                "                    <IBAN>JO93JGBA6010000290450010010000</IBAN>\n" +
                "                </Id>\n" +
                "            </DbtrAcct>\n" +
                "            <DbtrAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>JGBAJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </DbtrAgt>\n" +
                "            <CdtrAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </CdtrAgt>\n" +
                "            <Cdtr>\n" +
                "                <Nm>MAEN SAMI HATTAR SALEM</Nm>\n" +
                "                <PstlAdr>\n" +
                "                    <AdrLine>Amman Jordan SLT JO SLT</AdrLine>\n" +
                "                </PstlAdr>\n" +
                "            </Cdtr>\n" +
                "            <CdtrAcct>\n" +
                "                <Id>\n" +
                "                    <IBAN>JO83HBHO0320000033330600101001</IBAN>\n" +
                "                </Id>\n" +
                "            </CdtrAcct>\n" +
                "        </CdtTrfTxInf>\n" +
                "    </FIToFICstmrCdtTrf>\n" +
                "</Document>";
    }

}
