package com.progressoft.jip;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class ResponseGenerator {
    private RejectionMotives reason;
    Pacs002Types response;

    public Pacs002 generate(Pacs008 pacs008, RejectionMotives reason, String reasonDesc) {
        validateArgs(pacs008, reason, reasonDesc);
        Pacs002 pacs002 = new Pacs002();
        response = setResponseType(reason, reasonDesc);

        setGroupHeader(pacs008, pacs002);
        setOriginalGroupInfo(pacs008, response, pacs002);
        setTransactionInfo(pacs008, pacs002);

        if (isNAUT(response)) setRejectionMotive(pacs002, reasonDesc);

        return pacs002;
    }

    public Pacs002Types getResponseType() {
        return response;
    }

    private void validateArgs(Pacs008 pacs008, RejectionMotives reason, String reasonDesc) {
        if (isNull(pacs008)) throw new NullPointerException("Pacs008 cannot be null");
        if (isNull(reason) && !isNull(reasonDesc))
            throw new IllegalArgumentException("shouldn't be a reason desc because its AUTH pacs002");
        if (!isNull(reason) && isNull(reasonDesc))
            throw new IllegalArgumentException("there should be a description for the rejection");
    }

    private boolean isNull(Object object) {
        return object == null;
    }

    private void setRejectionMotive(Pacs002 pacs002, String reasonDesc) {
        pacs002.setRejectionReson(String.valueOf(reason));
        pacs002.setRejectionAdditionalInfo(reasonDesc);
    }

    private boolean isNAUT(Pacs002Types response) {
        return response == Pacs002Types.NAUT;
    }

    private void setTransactionInfo(Pacs008 pacs008, Pacs002 pacs002) {
        pacs002.setOriginalInstructionIdentification(pacs008.getInstrId());
        pacs002.setOriginalEndToEndIdentification(pacs008.getEndToEndId());
        pacs002.setOriginalTransactionIdentification(pacs008.getTxtId());
        pacs002.setInstructgAgentBICFI(pacs008.getDebtorBICFI());
        pacs002.setInstructdAgentBICFI(pacs008.getCreditorBICFI());
        pacs002.setInterbankSettlementDate(pacs008.getIntrBkSttlmDt());
        pacs002.setCurrency(pacs008.getCurrency());
        pacs002.setAmount(pacs008.getAmount());
    }

    private void setGroupHeader(Pacs008 pacs008, Pacs002 pacs002) {
        setMessageId(pacs008, pacs002);
        setCreationDate(pacs002);

        pacs002.setInstructingAgentBICFI(pacs008.getCreditorBICFI());
        pacs002.setInstructedMemberIdentification("ZYAAJOA0AIPS");
    }

    private void setMessageId(Pacs008 pacs008, Pacs002 pacs002) {
        MessageIdGenerator generator = new MessageIdGenerator();
        String msgID = generator.generate(pacs008.getInstructedBICFI());

        pacs002.setMessageId(msgID);
    }

    private void setCreationDate(Pacs002 pacs002) {
        LocalDate localDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        String localTime = LocalTime.now().format(formatter);

        pacs002.setOriginalCreationDateTime(localDate + "T" + localTime + "Z");
    }

    private void setOriginalGroupInfo(Pacs008 pacs008, Pacs002Types response, Pacs002 pacs002) {
        pacs002.setOriginalMessageIdentification(pacs008.getMessageID());
        pacs002.setOriginalMessageNameIdentification("pacs.008.001.08");
        pacs002.setResponseType(String.valueOf(response));
    }


    private Pacs002Types setResponseType(RejectionMotives reason, String desc) {
        if (isNull(reason) && isNull(desc))
            response = Pacs002Types.AUTH;
        else if (!isNull(reason) && !isNull(desc)) {
            this.reason = reason;
            response = Pacs002Types.NAUT;
        }

        return response;
    }
}
