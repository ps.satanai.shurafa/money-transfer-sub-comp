package com.progressoft;

import javax.xml.bind.annotation.XmlElement;

public class PaymentIdentification {
    private String InstrId;
    private String EndToEndId;
    private String TxId;

    @XmlElement(name = "InstrId")
    public void setInstrId(String instrId) {
        InstrId = instrId;
    }

    @XmlElement(name = "EndToEndId")
    public void setEndToEndId(String endToEndId) {
        EndToEndId = endToEndId;
    }

    @XmlElement(name = "TxId")
    public void setTxId(String txId) {
        TxId = txId;
    }

    public String getInstrId() {
        return InstrId;
    }

    public String getEndToEndId() {
        return EndToEndId;
    }

    public String getTxId() {
        return TxId;
    }
}
