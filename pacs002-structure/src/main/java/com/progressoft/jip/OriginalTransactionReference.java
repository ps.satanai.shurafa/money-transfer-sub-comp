package com.progressoft.jip;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"intrBkSttlmAmt",
        "interbankSettlementDate"
})
public class OriginalTransactionReference {
    private String interbankSettlementDate;
    private InterbankSettlementAmount intrBkSttlmAmt;

    @XmlElement(name = "IntrBkSttlmDt")
    public void setInterbankSettlementDate(String interbankSettlementDate) {
        this.interbankSettlementDate = interbankSettlementDate;
    }

    @XmlElement(name = "IntrBkSttlmAmt")
    public void setIntrBkSttlmAmt(InterbankSettlementAmount intrBkSttlmAmt) {
        this.intrBkSttlmAmt = intrBkSttlmAmt;
    }

    public String getInterbankSettlementDate() {
        return interbankSettlementDate;
    }

    public InterbankSettlementAmount getIntrBkSttlmAmt() {
        return intrBkSttlmAmt;
    }
}