package com.progressoft.jip;

import com.progressoft.jip.entities.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class IntegrationTest {
    private MessagesRepository messagesRepository;
    private AccountsRepository accountsRepository;
    private ParticipantsRepository participantsRepository;
    private PurposeCodesRepository purposeCodesRepository;
    private CurrenciesRepository currenciesRepository;
    private Pacs008Handler handler;
    private PacsController controller;

    @Before
    public void setUp() {
        Pacs002Serializer serializer = new Pacs002Serializer();
        Pacs008Parser parser = new Pacs008Parser();

        messagesRepository = mock(MessagesRepository.class);
        accountsRepository = mock(AccountsRepository.class);
        participantsRepository = mock(ParticipantsRepository.class);
        purposeCodesRepository = mock(PurposeCodesRepository.class);
        currenciesRepository = mock(CurrenciesRepository.class);

        Pacs008ValuesValidator validator = new Pacs008ValuesValidator(messagesRepository, participantsRepository,
                accountsRepository, currenciesRepository, purposeCodesRepository);

        handler = new Pacs008Handler(serializer, parser, validator, messagesRepository);

        mockRepositoriesMethods();
    }

    private void mockRepositoriesMethods() {
        when(participantsRepository.existsById("JGBAJOA0")).thenReturn(true);
        when(participantsRepository.existsById("HBHOJOA0")).thenReturn(true);
        when(accountsRepository.existsById("JO93JGBA6010000290450010010000")).thenReturn(true);
        when(accountsRepository.existsById("JO83HBHO0320000033330600101001")).thenReturn(true);
        when(purposeCodesRepository.existsById("11110")).thenReturn(true);
        when(currenciesRepository.existsById("JOD")).thenReturn(true);
    }


    @Test
    public void givenValidPacs008_whenSendPacsToController_ThenShouldReturnAuthResponse() {
        when(messagesRepository.existsById("JGBA9991023754121315560")).thenReturn(false);
        controller = new PacsController(handler);

        String actualResponse = controller.getResponse(getValidPacs());
        String includedTags = getValidPacsIncludedTags();
        Assert.assertTrue(actualResponse.contains(includedTags));
    }

    @Test
    public void givenValidPacs008WithDuplicateID_whenSendPacsToController_ThenShouldReturnNautResponse() {
        when(messagesRepository.existsById("JGBA9991023754121315560")).thenReturn(true);
        controller = new PacsController(handler);

        String actualResponse = controller.getResponse(getValidPacs());
        String includedTags = getDuplicateMsgIdPacsIncludedTags();
        Assert.assertTrue(actualResponse.contains(includedTags));
    }

    @Test
    public void givenEmptyMessage_whenSendPacsToController_ThenShouldReturnErrorMessage() {
        controller = new PacsController(handler);

        String actualResponse = controller.getResponse(null);
        String expectedResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<Error>THE MESSAGE CANNOT BE EMPTY!!!</Error>";
        Assert.assertEquals(expectedResponse, actualResponse);
    }

    private String getDuplicateMsgIdPacsIncludedTags() {
        return "            <StsRsnInf>\n" +
                "                <Rsn>\n" +
                "                    <Prtry>FF03</Prtry>\n" +
                "                </Rsn>\n" +
                "                <AddtlInf>Duplicate message ID</AddtlInf>\n" +
                "            </StsRsnInf>\n";
    }

    private String getValidPacsIncludedTags() {
        return "        <OrgnlGrpInfAndSts>\n" +
                "            <OrgnlMsgId>JGBA9991023754121315560</OrgnlMsgId>\n" +
                "            <OrgnlMsgNmId>pacs.008.001.08</OrgnlMsgNmId>\n" +
                "            <StsRsnInf>\n" +
                "                <Rsn>\n" +
                "                    <Prtry>AUTH</Prtry>\n" +
                "                </Rsn>\n" +
                "            </StsRsnInf>\n" +
                "        </OrgnlGrpInfAndSts>\n";
    }

    private String getValidPacs() {
        return "<Document xmlns=\"urn:iso:std:iso:20022:tech:xsd:pacs.008.001.10\">\n" +
                "    <FIToFICstmrCdtTrf>\n" +
                "        <GrpHdr>\n" +
                "            <MsgId>JGBA9991023754121315560</MsgId>\n" +
                "            <CreDtTm>2020-12-28T07:47:50Z</CreDtTm>\n" +
                "            <NbOfTxs>1</NbOfTxs>\n" +
                "            <SttlmInf>\n" +
                "                <SttlmMtd>CLRG</SttlmMtd>\n" +
                "            </SttlmInf>\n" +
                "        </GrpHdr>\n" +
                "        <CdtTrfTxInf>\n" +
                "            <PmtId>\n" +
                "                <InstrId>JGBA2810090750927334298</InstrId>\n" +
                "                <EndToEndId>NOTPROVIDED</EndToEndId>\n" +
                "                <TxId>JGBA2812094750927334297</TxId>\n" +
                "            </PmtId>\n" +
                "            <PmtTpInf>\n" +
                "                <ClrChanl>RTNS</ClrChanl>\n" +
                "                <SvcLvl>\n" +
                "                    <Prtry>0100</Prtry>\n" +
                "                </SvcLvl>\n" +
                "                <LclInstrm>\n" +
                "                    <Prtry>CSDC</Prtry>\n" +
                "                </LclInstrm>\n" +
                "                <CtgyPurp>\n" +
                "                    <Prtry>11110</Prtry>\n" +
                "                </CtgyPurp>\n" +
                "            </PmtTpInf>\n" +
                "            <IntrBkSttlmAmt Ccy=\"JOD\">1</IntrBkSttlmAmt>\n" +
                "            <IntrBkSttlmDt>2022-08-04</IntrBkSttlmDt>\n" +
                "            <ChrgBr>SLEV</ChrgBr>\n" +
                "            <InstgAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>JGBAJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstgAgt>\n" +
                "            <InstdAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </InstdAgt>\n" +
                "            <Dbtr>\n" +
                "                <Nm>NISREEN MOHAMMAD YOUSEF</Nm>\n" +
                "                <PstlAdr>\n" +
                "                    <AdrLine>new zarqa</AdrLine>\n" +
                "                </PstlAdr>\n" +
                "            </Dbtr>\n" +
                "            <DbtrAcct>\n" +
                "                <Id>\n" +
                "                    <IBAN>JO93JGBA6010000290450010010000</IBAN>\n" +
                "                </Id>\n" +
                "            </DbtrAcct>\n" +
                "            <DbtrAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>JGBAJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </DbtrAgt>\n" +
                "            <CdtrAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                </FinInstnId>\n" +
                "            </CdtrAgt>\n" +
                "            <Cdtr>\n" +
                "                <Nm>MAEN SAMI HATTAR SALEM</Nm>\n" +
                "                <PstlAdr>\n" +
                "                    <AdrLine>Amman Jordan SLT JO SLT</AdrLine>\n" +
                "                </PstlAdr>\n" +
                "            </Cdtr>\n" +
                "            <CdtrAcct>\n" +
                "                <Id>\n" +
                "                    <IBAN>JO83HBHO0320000033330600101001</IBAN>\n" +
                "                </Id>\n" +
                "            </CdtrAcct>\n" +
                "        </CdtTrfTxInf>\n" +
                "    </FIToFICstmrCdtTrf>\n" +
                "</Document>";
    }

}

