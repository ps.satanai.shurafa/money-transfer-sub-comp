package com.progressoft;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;

public class Main {
    public static void main(String[] args) throws JAXBException {

        String xml = "<Document>" +
                "    <FIToFICstmrCdtTrf>\n" +
                "        <GrpHdr>\n" +
                "            <MsgId>JGBA2812094750927334298</MsgId>\n" +
                "            Unique message id\n" +
                "            <CreDtTm>2020-12-28T07:47:50Z</CreDtTm>\n" +
                "            <NbOfTxs>1</NbOfTxs>\n" +
                "            Fixed\n" +
                "            <SttlmInf>\n" +
                "                <SttlmMtd>CLRG</SttlmMtd>\n" +
                "            </SttlmInf>\n" +
                "        </GrpHdr>\n" +
                "        <CdtTrfTxInf>\n" +
                "            <PmtId>\n" +
                "                <InstrId>JGBA2812094750927334298</InstrId>\n" +
                "                <EndToEndId>NOTPROVIDED</EndToEndId>\n" +
                "                <TxId>JGBA2812094750927334297</TxId>\n" +
                "            </PmtId>\n" +
                "            <PmtTpInf>\n" +
                "                <ClrChanl>RTNS</ClrChanl>\n" +
                "                Fixed\n" +
                "                <SvcLvl>\n" +
                "                    <Prtry>0100</Prtry>\n" +
                "                    Fixed\n" +
                "                </SvcLvl>\n" +
                "                <LclInstrm>\n" +
                "                    <Prtry>CSDC</Prtry>\n" +
                "                    Fixed\n" +
                "                </LclInstrm>\n" +
                "                <CtgyPurp>\n" +
                "                    <Prtry>11110</Prtry>\n" +
                "                </CtgyPurp>\n" +
                "            </PmtTpInf>\n" +
                "            <IntrBkSttlmAmt Ccy=\"JOD\">1</IntrBkSttlmAmt>\n" +
                "            <IntrBkSttlmDt>2021-10-09</IntrBkSttlmDt>\n" +
                "            <ChrgBr>SLEV</ChrgBr>\n" +
                "            <InstgAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>JGBAJOA0</BICFI>\n" +
                "                    Sending participant (Debtor)\n" +
                "                </FinInstnId>\n" +
                "            </InstgAgt>\n" +
                "            <InstdAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                    Receiving participant (creditor)\n" +
                "                </FinInstnId>\n" +
                "            </InstdAgt>\n" +
                "            <Dbtr>\n" +
                "                Debtor information\n" +
                "                <Nm>NISREEN MOHAMMAD YOUSEF</Nm>\n" +
                "                <PstlAdr>\n" +
                "                    <AdrLine>new zarqa</AdrLine>\n" +
                "                </PstlAdr>\n" +
                "            </Dbtr>\n" +
                "            <DbtrAcct>\n" +
                "                <Id>\n" +
                "                <IBAN>JO93JGBA6010000290450010010000</IBAN>\n" +
                "                </Id>\n" +
                "                Debtor IBAN\n" +
                "            </DbtrAcct>\n" +
                "            <DbtrAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>JGBAJOA0</BICFI>\n" +
                "                    Debtor agent, should always be as InstgAgt\n" +
                "                </FinInstnId>\n" +
                "            </DbtrAgt>\n" +
                "            <CdtrAgt>\n" +
                "                <FinInstnId>\n" +
                "                    <BICFI>HBHOJOA0</BICFI>\n" +
                "                    Creditor agent, should always be as InstdAgnt\n" +
                "                </FinInstnId>\n" +
                "            </CdtrAgt>\n" +
                "            <Cdtr>\n" +
                "                Creditor information\n" +
                "                <Nm>MAEN SAMI HATTAR SALEM</Nm>\n" +
                "                <PstlAdr>\n" +
                "                    <AdrLine>Amman Jordan SLT JO SLT</AdrLine>\n" +
                "                    Could have a repetition with multiple address\n" +
                "                    lines\n" +
                "                </PstlAdr>\n" +
                "            </Cdtr>\n" +
                "            <CdtrAcct>\n" +
                "                <Id>\n" +
                "                    <IBAN>JO83HBHO0320000033330600101001</IBAN>\n" +
                "                </Id>\n" +
                "                Creditor IBAN\n" +
                "            </CdtrAcct>\n" +
                "        </CdtTrfTxInf>\n" +
                "    </FIToFICstmrCdtTrf>\n" +
                "</Document>";
        JAXBContext jaxbContext = JAXBContext.newInstance(Pacs8Doc.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        Pacs8Doc root = (Pacs8Doc) unmarshaller.unmarshal(new StringReader(xml));
        System.out.println(root.getfIToFICstmrCdtTrf().getGroupHeader().getMessageId());
        System.out.println(root.getfIToFICstmrCdtTrf().getCdtTrfTxInf().getDebtorAccount().getAccountId().getIBAN());
        System.out.println(root.getfIToFICstmrCdtTrf().getCdtTrfTxInf().getPmtTpInf().getClrChanl());
        Marshaller marshaller = jaxbContext.createMarshaller();
        StringWriter sw = new StringWriter();
        marshaller.marshal(root,sw);

        System.out.println(sw.toString());
    }
}
