package com.progressoft.jip.entities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PurposeCodesRepository extends JpaRepository<PurposeCodes,String> {
}
