package com.progressoft.jip;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"clrSysMmbId",
        "BICFI"
})
public class FinancialInstitutionIdentification {
    private String BICFI;
    private ClearingSystemMemberIdentification clrSysMmbId;

    @XmlElement(name = "ClrSysMmbId")
    public void setClrSysMmbId(ClearingSystemMemberIdentification clrSysMmbId) {
        this.clrSysMmbId = clrSysMmbId;
    }

    @XmlElement(name = "BICFI")
    public void setBICFI(String BICFI) {
        this.BICFI = BICFI;
    }

    public String getBICFI() {
        return BICFI;
    }

    public ClearingSystemMemberIdentification getClrSysMmbId() {
        return clrSysMmbId;
    }
}
