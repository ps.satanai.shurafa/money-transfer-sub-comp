package com.progressoft;

import javax.xml.bind.annotation.XmlElement;

public class Account {
    private AccountId accountId;

    @XmlElement(name = "Id")
    public void setAccountId(AccountId accountId) {
        this.accountId = accountId;
    }

    public AccountId getAccountId() {
        return accountId;
    }
}
