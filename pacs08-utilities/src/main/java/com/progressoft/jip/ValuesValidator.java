package com.progressoft.jip;

import com.progressoft.jip.entities.MessagesRepository;

public interface ValuesValidator {
    RejectionMotives validate(Pacs008 pacs008);
    String getReasonDesc();
    boolean isDuplicateMsg();
}
