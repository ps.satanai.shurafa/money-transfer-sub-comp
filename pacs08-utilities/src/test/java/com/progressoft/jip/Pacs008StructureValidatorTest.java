package com.progressoft.jip;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Scanner;

public class Pacs008StructureValidatorTest {
    @Test
    public void can_create() {
        new Pacs008StructureValidator();
    }

    @Test
    public void givenNullXml_whenValidate_thenThrowException() {
        Pacs008StructureValidator validator = new Pacs008StructureValidator();
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> validator.validate(null));
        Assertions.assertEquals("Null xml", exception.getMessage());

    }

    @Test
    public void givenEmptyXml_whenValidate_thenThrowException() {
        Pacs008StructureValidator validator = new Pacs008StructureValidator();
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> validator.validate(""));
        Assertions.assertEquals("Empty xml", exception.getMessage());
    }

    @Test
    public void givenValidXml_whenValidate_thenShouldWriteTheXmlInValidationFile() throws URISyntaxException {
        Pacs008StructureValidator validator = new Pacs008StructureValidator();
        validator.validate(getXml());
        URL res = Pacs008StructureValidator.class.getClassLoader().getResource("structureValidation/xmlToValidate.xml");

        File file = null;
        if (res != null) {
            file = new File(String.valueOf(Paths.get(res.toURI()).toFile()));
        }
        String expectedResult = getExpectedResult(file);

        Assertions.assertEquals(getXml(), expectedResult);
    }

    private String getExpectedResult(File file) {
        StringBuilder result = new StringBuilder();
        try (Scanner myReader = new Scanner(file)) {
            while (myReader.hasNextLine()) {
                result.append(myReader.nextLine());
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            e.printStackTrace();
        }
        return result.toString();
    }

    private String getXml() {
        return "<Document xmlns=\"urn:iso:std:iso:20022:tech:xsd:pacs.008.001.10\">" +
                "<FIToFICstmrCdtTrf>" +
                "<GrpHdr>" +
                "<MsgId>JGBA1234594750927338882</MsgId>" +
                "<CreDtTm>2020-12-28T07:47:50Z</CreDtTm>" +
                "<NbOfTxs>1</NbOfTxs>" +
                "<SttlmInf>" +
                "<SttlmMtd>CLRG</SttlmMtd>" +
                "</SttlmInf>" +
                "</GrpHdr>" +
                "<CdtTrfTxInf>" +
                "<PmtId>" +
                "<InstrId>JGBA2812094750927334298</InstrId>" +
                "<EndToEndId>NOTPROVIDED</EndToEndId>" +
                "<TxId>JGBA2812094750927334297</TxId>" +
                "</PmtId>" +
                "<PmtTpInf>" +
                "<ClrChanl>RTNS</ClrChanl>" +
                "<SvcLvl>" +
                "<Prtry>0100</Prtry>" +
                "</SvcLvl>" +
                "<LclInstrm>" +
                "<Prtry>CSDC</Prtry>" +
                "</LclInstrm>" +
                "<CtgyPurp>" +
                "<Prtry>11110</Prtry>" +
                "</CtgyPurp>" +
                "</PmtTpInf>" +
                "<IntrBkSttlmAmt Ccy=\"JOD\">1</IntrBkSttlmAmt>" +
                "<IntrBkSttlmDt>2022-06-30</IntrBkSttlmDt>" +
                "<ChrgBr>SLEV</ChrgBr>" +
                "<InstgAgt>" +
                "<FinInstnId>" +
                "<BICFI>JGBAJOA0</BICFI>" +
                "</FinInstnId>" +
                "</InstgAgt>" +
                "<InstdAgt>" +
                "<FinInstnId>" +
                "<BICFI>HBHOJOA0</BICFI>" +
                "</FinInstnId>" +
                "</InstdAgt>" +
                "<Dbtr>" +
                "<Nm>NISREEN MOHAMMAD YOUSEF</Nm>" +
                "<PstlAdr>" +
                "<AdrLine>new zarqa</AdrLine>" +
                "</PstlAdr>" +
                "</Dbtr>" +
                "<DbtrAcct>" +
                "<Id>" +
                "<IBAN>JO93JGBA6010000290450010010000</IBAN>" +
                "</Id>" +
                "</DbtrAcct>" +
                "<DbtrAgt>" +
                "<FinInstnId>" +
                "<BICFI>JGBAJOA0</BICFI>" +
                "</FinInstnId>" +
                "</DbtrAgt>" +
                "<CdtrAgt>" +
                "<FinInstnId>" +
                "<BICFI>HBHOJOA0</BICFI>" +
                "</FinInstnId>" +
                "</CdtrAgt>" +
                "<Cdtr>" +
                "<Nm>MAEN SAMI HATTAR SALEM</Nm>" +
                "<PstlAdr>" +
                "<AdrLine>Amman Jordan SLT JO SLT</AdrLine>" +
                "</PstlAdr>" +
                "</Cdtr>" +
                "<CdtrAcct>" +
                "<Id>" +
                "<IBAN>JO83HBHO0320000033330600101001</IBAN>" +
                "</Id>" +
                "</CdtrAcct>" +
                "</CdtTrfTxInf>" +
                "</FIToFICstmrCdtTrf>" +
                "</Document>";
    }


}
