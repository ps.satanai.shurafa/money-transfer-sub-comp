package com.progressoft.jip.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "rejectionMotives")
public class RejectionMotives {

    @Id
    @Column(
            name = "Code",
            columnDefinition = "TEXT",
            unique = true
    )
    private String code;
    @Column(
            name = "description",
            columnDefinition = "TEXT"
    )
    private String description;

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
