package com.progressoft.jip;

import com.progressoft.jip.entities.*;

import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class Pacs008ValuesValidator implements ValuesValidator {
    private RejectionMotives reason;
    private String reasonDesc;
    private boolean isDuplicateMsg;
    private final MessagesRepository messagesRepository;
    private final ParticipantsRepository participantsRepository;
    private final AccountsRepository accountsRepository;
    private final CurrenciesRepository currenciesRepository;
    private final PurposeCodesRepository purposeCodesRepository;
    private final Logger logger = Logger.getLogger(Pacs008ValuesValidator.class.getName());

    public Pacs008ValuesValidator(MessagesRepository messages, ParticipantsRepository participants, AccountsRepository accounts,
                                  CurrenciesRepository currencies, PurposeCodesRepository purposeCodes) {
        initVariables();
        messagesRepository = messages;
        participantsRepository = participants;
        accountsRepository = accounts;
        currenciesRepository = currencies;
        purposeCodesRepository = purposeCodes;
    }

    private void initVariables() {
        reason = null;
        reasonDesc = null;
        isDuplicateMsg = false;
    }

    @Override
    public boolean isDuplicateMsg() {
        return isDuplicateMsg;
    }

    @Override
    public RejectionMotives validate(Pacs008 pacs008) {
        initVariables();

        if (!isDuplicateMsgId(pacs008)) {
            msgIdValidator(pacs008);
            settlementDateValidator(pacs008);
            namesValidator(pacs008);
            addressValidator(pacs008);
            amountValidator(pacs008);
            fixedValuesValidator(pacs008);
            bicValidator(pacs008);
            ibanValidator(pacs008);
            purposeCodeValidator(pacs008);
            currencyValidator(pacs008);

        }
        return reason;
    }

    @Override
    public String getReasonDesc() {
        return reasonDesc;
    }

    private boolean isDuplicateMsgId(Pacs008 pojo) {
        String msgId = pojo.getMessageID();

        if (isExistMsgID(msgId) && isNull(reason)) {
            isDuplicateMsg = true;
            reason = RejectionMotives.FF03;
            reasonDesc = "Duplicate message ID";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);

            return true;
        }

        return false;
    }

    private void fixedValuesValidator(Pacs008 pacs008) {
        int numberOfTransactions = pacs008.getNumberOfTransactions();
        String settlementMethod = pacs008.getSettlementMethod();
        String clearChanl = pacs008.getClrChanl();
        String serviceLevel = pacs008.getServiceLevel();
        String localInstrument = pacs008.getLocalInstrument();
        String chargeBearer = pacs008.getChargeBearer();

        if (numberOfTransactions != 1 && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "Invalid number of transaction";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        } else if (!settlementMethod.equals("CLRG") && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "Invalid settlement method";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        } else if (!clearChanl.equals("RTNS") && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "Invalid Clear Chanl";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        } else if (!serviceLevel.equals("0100") && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "Invalid number of service level";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        } else if (!localInstrument.equals("CSDC") && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "Invalid number of local instrument";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        } else if (!chargeBearer.equals("SLEV") && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "Invalid ChrgBr";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }
    }

    private void amountValidator(Pacs008 pacs008) {
        String amount = pacs008.getAmount();

        if (!isFormatted(amount) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "Invalid amount format";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        } else if (isNegative(amount) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "The amount cannot be negative";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        } else if (isZero(amount) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "The amount cannot be zero";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }

    }

    private boolean isFormatted(String amount) {
        return Pattern.matches("[0-9]{1,5}([.]{1}[0-9]{2})?", amount);
    }

    private boolean isZero(String amount) {
        return Integer.parseInt(amount) == 0;
    }

    private boolean isNegative(String amount) {
        return Integer.parseInt(amount) < 0;
    }

    private void currencyValidator(Pacs008 pojo) {
        String currency = pojo.getCurrency();

        if (!isValidCurrency(currency) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "Invalid currency";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }
        if (!isAvailableCurrency(currency) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "The currency:" + currency + " is not Available";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }
    }

    private boolean isAvailableCurrency(String currency) {
        return currenciesRepository.existsById(currency);
    }

    private boolean isNull(RejectionMotives reason) {
        return reason == null;
    }

    private boolean isValidCurrency(String currency) {
        return Pattern.matches("[A-Z]{2,6}", currency);
    }

    private void purposeCodeValidator(Pacs008 pojo) {
        String purposeCode = pojo.getCategoryPurpose();

        if (!isPurposeCode(purposeCode) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "Invalid purpose code";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }
        if (!isExistPurposeCode(purposeCode) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "Purpose code: " + purposeCode + " doesn't exist";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }
    }

    private boolean isExistPurposeCode(String purposeCode) {
        return purposeCodesRepository.existsById(purposeCode);
    }


    private boolean isPurposeCode(String purposeCode) {
        return Pattern.matches("[0-9]{5}", purposeCode);
    }

    private void bicValidator(Pacs008 pojo) {
        String instgAtBIC = pojo.getInstructingBICFI();
        String instdAtBIC = pojo.getInstructedBICFI();
        String debtorBIC = pojo.getDebtorBICFI();
        String creditorBIC = pojo.getCreditorBICFI();

        if (isNotValidBIC(instdAtBIC) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "The instructed agent BIC is invalid";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);

        }
        if (isNotValidBIC(instgAtBIC) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "The instructing agent BIC is invalid";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }
        if (isNotValidBIC(debtorBIC) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "The debtor BIC is invalid";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }
        if (isNotValidBIC(creditorBIC) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "The creditor BIC is invalid";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }
        if (isNotExistBIC(instgAtBIC) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "The instructing agent BIC doesn't exist";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }
        if (isNotExistBIC(instdAtBIC) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "The instructed agent BIC doesn't exist";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }
        if (isNotExistBIC(creditorBIC) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "The creditor BIC doesn't exist";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }
        if (isNotExistBIC(debtorBIC) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "The debtor agent BIC doesn't exist";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }
        if (notTheSameBIC(instdAtBIC, creditorBIC) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "The instructed agent BIC and the creditor BIC should be the same";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);

        }
        if (notTheSameBIC(instgAtBIC, debtorBIC) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "The instructing agent BIC and the debtor BIC should be the same";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }

    }

    private boolean isNotExistBIC(String instdAtBIC) {
        return !participantsRepository.existsById(instdAtBIC);
    }

    private boolean isNotValidBIC(String BIC) {
        return !Pattern.matches("[A-Z]{4}JOA0", BIC);
    }

    private boolean notTheSameBIC(String BIC1, String BIC2) {
        return !BIC1.equals(BIC2);
    }

    private void msgIdValidator(Pacs008 pojo) {
        String msgId = pojo.getMessageID();
        String instrId = pojo.getInstrId();
        String txId = pojo.getTxtId();

        if (isNotValidId(msgId) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "Invalid message ID, should consists og 23 char, first four are letters";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }

        if (isNotValidId(instrId) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "Invalid original instruction ID, should consists og 23 char, first four are letters";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }
        if (isNotValidId(txId) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "Invalid original transaction ID, should consists og 23 char, first four are letters";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }
    }

    private boolean isExistMsgID(String msgId) {
        return messagesRepository.existsById(msgId);
    }

    private boolean isNotValidId(String msgId) {
        return !Pattern.matches("[A-Z]{4}[0-9]{19,50}", msgId);
    }

    private void ibanValidator(Pacs008 pojo) {
        String debtorIBAN = pojo.getDebtorIBAN();
        String creditorIBAN = pojo.getCreditorIBAN();
        String debtorBIC = pojo.getDebtorBICFI();
        String creditorBIC = pojo.getCreditorBICFI();

        if (isNotValidIBAN(debtorIBAN) && isNull(reason)) {
            reason = RejectionMotives.AC02;
            reasonDesc = "Invalid debtor IBAN";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }
        if (isNotValidIBAN(creditorIBAN)) {
            reason = RejectionMotives.AC03;
            reasonDesc = "Invalid creditor IBAN";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }
        if (isNotExistAccount(debtorIBAN, debtorBIC) && isNull(reason)) {
            reason = RejectionMotives.AC02;
            reasonDesc = "Debtor account doesn't exist";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }
        if (isNotExistAccount(creditorIBAN, creditorBIC) && isNull(reason)) {
            reason = RejectionMotives.AC03;
            reasonDesc = "Creditor account doesn't exist";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }
    }

    private boolean isNotExistAccount(String IBAN, String BIC) {
        return !accountsRepository.existsById(IBAN);
    }

    private boolean isNotValidIBAN(String iban) {
        return !Pattern.matches("^JO\\d{2}[A-Z]{4}\\d{4}[A-Z0-9]{18}$", iban);
    }

    private void addressValidator(Pacs008 pojo) {
        String debtorAddress = pojo.getDebtorAddress();
        String creditorAddress = pojo.getCreditorAddress();

        if (numOfPartsIsLessThan(debtorAddress.trim(), 2) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "Invalid debtor address, should be at least two parts";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }
        if (numOfPartsIsLessThan(creditorAddress.trim(), 2) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "Invalid creditor address, should be at least two parts";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }

        if (isNotWord(debtorAddress.trim()) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "Invalid debtor address, should contains letters only";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }

        if (isNotWord(creditorAddress.trim()) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "Invalid creditor address, should contains letters only";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }
    }

    private void namesValidator(Pacs008 pojo) {
        String debtorName = pojo.getDebtorName();
        String creditorName = pojo.getCreditorName();

        if (numOfPartsIsLessThan(debtorName.trim(), 3) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "Invalid debtor name, should be at least three parts";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }

        if (numOfPartsIsLessThan(creditorName.trim(), 3) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "Invalid creditor name, should be at least three parts";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }

        if (isNotWord(debtorName.trim()) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "Invalid debtor name, should contains letters only";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }

        if (isNotWord(creditorName.trim()) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "Invalid creditor name, should contains letters only";
            logger.log(Level.WARNING, "Rejection cause: " + reasonDesc);
        }
    }

    private boolean isNotWord(String word) {
        return !Pattern.matches("[aA-zZ ]{2,}", word);
    }

    private boolean numOfPartsIsLessThan(String name, int partsNum) {
        int spaceCounter = 0;
        boolean previousIsSpace = false;

        for (int i = 0; i < name.length(); i++) {
            if (isSpace(String.valueOf(name.charAt(i))) && !previousIsSpace) {
                spaceCounter++;
                previousIsSpace = true;
            } else
                previousIsSpace = false;

        }
        return spaceCounter + 1 < partsNum;
    }

    private boolean isSpace(String character) {
        return character.equals(" ");
    }

    private void settlementDateValidator(Pacs008 pojo) {
        String settlementDate = pojo.getIntrBkSttlmDt();
        LocalDate currentDate = java.time.LocalDate.now();

        if (!settlementDate.equals(currentDate.toString()) && isNull(reason)) {
            reason = RejectionMotives.FF03;
            reasonDesc = "Invalid settlement date";
            logger.log(Level.WARNING, "Rejection cause: Invalid settlement date:" + settlementDate + ", should be:" + currentDate);
        }
    }

}
